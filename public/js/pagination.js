$url = ""
$params = {}
$totalPage = 1
$totalPerPage = 1

$filter_ajax = null

function setupSearchListener(){
    $('.input-search').keyup(throttle(function (e) { 
        if (e.keyCode != '91') {
            if ($(this).val() != ""){
                $isEmpty = false;
            }
      
            if (!$isEmpty){
                $params['search_query'] = $(this).val();
                $totalPage = 1
                $params['current_page'] = 1;
    
                clearListGroup();
                filterData();
            }
    
            if ($(this).val() == ""){
                $isEmpty = true;
            }
            if($filter_ajax){
                $filter_ajax.abort();
            }
        }
    }));
}

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
            f.apply(context, args);
        },
        delay || 250);
    };
}

function setupDatePickerFilter(){
    // Setup start date picker
    $(document).ready(function(){
        $('input[name="start_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top left",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $params['start_date'] = $(this).val();
            $params['current_page'] = 1;
            clearListGroup();
            filterData()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth(), 1));
        $params['start_date'] = $('input[name="start_date"]').val();

        $('input[name="end_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top right",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $params['end_date'] = $(this).val();
            $params['current_page'] = 1;
            clearListGroup();
            filterData()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0));
        $params['end_date'] = $('input[name="end_date"]').val();
    });
}

function setupPagination(){
    $('.btn-page, .btn-prev, .btn-next').click(function (e) { 
        $params['current_page'] = $(this).data('page');

        clearListGroup();
        filterData()
    });
}

function clearListGroup(){
    $('#list-group').empty();
    $('#tbody-shimmer').removeClass('d-none')
}

function filterData(){
    $params['total_page'] = $totalPage;
    $params['total_per_page'] = $totalPerPage;

    console.log($params);

    if ($url != ""){
        $.ajax({
            type: "GET",
            url: location.origin + "/uberbiz_admin/public/pagination_update",
            data: $params,
            dataType: "text",
            success: function (response) {
                $('.container-pagination').html(response);
                setupPagination();

                $filter_ajax = $.ajax({
                    type: "GET",
                    url: $url,
                    data: $params,
                    dataType: "text",
                    success: function (response) {
                        $json = JSON.parse(response);
                        $('.container-pagination').html($json.pagination);
                        $totalPage = $json.total_page;
                        setupPagination();
                        $('#tbody-shimmer').addClass('d-none')
                        $('#list-group').html($json.items);

                        didFilterDataFinish();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                        console.log(thrownError);
                    }
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
                console.log(thrownError);
            }
        });
    }else{
        alert('URL is missing');
    }  
}

function didFilterDataFinish(){

}