<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use App\Helpers\QBHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ChatController extends Controller
{

    private $pagination = 1;

    function create_session(Request $request){

        // CREATE SESSION
        $params = [
            "application_id" => 88693,
            "auth_key" => "YZmVsQba95WYcsH",
            "nonce" => "5",
            "timestamp" => time(),
        ];

        $params["signature"] = hash_hmac(
            'sha1', 
            "application_id=".$params["application_id"]."&auth_key=".$params["auth_key"]."&nonce=".$params["nonce"]."&timestamp=".$params["timestamp"],
            "Ep5pdFvXqMC7QZJ"
        );
        
        $response = (new QBHelper())->publicApi('session.json', $params, 'POST');
        $request->session()->put('user.qb_token', $response->session->token);

        // LOGIN TO QUICKBLOX
        $params = [
            "login" => "admin",
            "password" => "admin1234" 
        ];
        $response = (new QBHelper())->privateApi('login.json', $params, 'POST');
    }
    
    function index(Request $request){
        $this->create_session($request);
        $params = [
            "limit" => $this->pagination
        ];
        $response = (new QBHelper())->privateApi('chat/Dialog.json', $params, 'GET');

        $users = (new QBHelper())->privateApi('users.json', [], 'GET');
        $data = [
            "dialogs" => $response,
            "users" => $users->items
        ];
        return view('chats', $data); 
    }

    function filter(Request $request){
        $params = [
            "skip" => $this->pagination * ($request->current_page - 1),
            "limit" => $this->pagination
        ];

        if ($request->search_query){
            // Handle Search Query
            $users = (new QBHelper())->privateApi('users/by_full_name.json', [
                "full_name" => $request->search_query
            ], 'GET');
            $occupants_id = [];
            if (isset($users->items)){
                foreach ($users->items as $item) {
                    array_push($occupants_id, $item->user->id);
                }
                $params["occupants_ids[in]"] = implode (",", $occupants_id);
            }else{
                $params["occupants_ids[in]"] = 0;
            }
        }
        $dialogs = (new QBHelper())->privateApi('chat/Dialog.json', $params, 'GET');
        $users = (new QBHelper())->privateApi('users.json', [], 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($dialogs->total_entries/ $dialogs->limit), 
                'total_per_page' => $dialogs->limit,
                'current_page' => (($dialogs->skip/$dialogs->limit) + 1)
            ])->render(),
            "items" => view('items/chat_item', ["dialogs" => $dialogs, "users" => $users->items])->render(),
            "total_page" => ceil($dialogs->total_entries/ $dialogs->limit)
        ];
        return $data;
    }

    function detail(Request $request){
        $params = [
            "_id" => $request->id
        ];
        $dialog = (new QBHelper())->privateApi('chat/Dialog.json', $params, 'GET');

        $params = [
            "sort_desc" => "date_sent",
            "chat_dialog_id" => $request->id
        ];
        $messages = (new QBHelper())->privateApi('chat/Message.json', $params, 'GET');

        $users = (new QBHelper())->privateApi('users.json', [], 'GET');

        $data = [
            "dialog" => $dialog->items,
            "messages" => $messages,
            "users" => $users->items
        ];
        return view('chat_detail', $data); 
    }

    function create(Request $request){
        // $params = [
        //     "body" => [],
        //     "images" => [
        //         "image" => $request->banner
        //     ]
        // ];
        // $response = Helper::privateApi(NetworkDao::uploadBanner, $params, 'UPLOAD');
        // return redirect()->back()->with('response', $response);
    }

    function update(Request $request){
    }

    function delete(Request $request){
        $response = (new QBHelper())->privateApi('chat/Dialog/'.$request->id.','.$request->id.'.json', [], 'DELETE');
        return redirect()->back()->with('response', $response);
    }

    function delete_message(Request $request){
        $response = (new QBHelper())->privateApi('chat/Message/'.$request->message_id.','.$request->message_id.'.json', [], 'DELETE');
        return redirect()->back()->with('response', $response);
    }
}
