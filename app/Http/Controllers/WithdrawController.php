<?php

namespace App\Http\Controllers;

use App\Constants\Message;
use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class WithdrawController extends Controller
{
    private $paginate = 2;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getWithdraw, $params, 'GET');
        try {
            if ($response->success){
                $data = [
                    "withdraws" => $response->data
                ];
                return view('withdraws', $data);
            }else{
                $request->session()->now('response', $response);
                return view('withdraws');
            }
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => Message::SERVER_ERROR  
            ];
            $request->session()->now('response', json_decode(json_encode($response)));
            return view('withdraws');
        }
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "search" => $request->search_query,
            "page" => $request->current_page
        ];
        $response = Helper::privateApi(NetworkDao::getWithdraw, $params, 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/withdraw_item', ["withdraws" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function create(){
        echo "CREATE";
    }

    function update(Request $request){
        $params = [
            "seller_withdraw_id" => $request->id,
            "action" => $request->action
        ];
        $response = Helper::privateApi(NetworkDao::updateWithdraw, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }

    function delete(Request $request){
        echo "DELETE";
        // $params = [
        //     "item_category_id" => $request->id,
        // ];
        // $response = Helper::privateApi(NetworkDao::deleteCategory, $params, 'DELETE');
        // return redirect()->back()->with('response', $response);
    }
}
