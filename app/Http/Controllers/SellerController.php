<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class SellerController extends Controller
{

    private $paginate = 10;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getSeller, $params, 'GET');
        
        try {
            if ($response->success){
                $data = [
                    "sellers" => $response->data
                ];
                return view('seller_list', $data);
            }else{
                $request->session()->now('response', $response);
                return view('seller_list');
            }
            
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()    
            ];
            $request->session()->now('response', json_decode(json_encode($response)));
            return view('seller_list');
        }
    }

    function detail(Request $request){
        $params = [
            "user_id" => $request->id
        ];
        $response = Helper::privateApi(NetworkDao::getSeller, $params, 'GET');
        $provinces = Helper::privateApi(NetworkDao::getProvinces, [], 'GET');


        $cities = Helper::privateApi(NetworkDao::getCities, ["province_id" => $response->data->store_address->province_id], 'GET');

        $data = [
            "seller" => $response->data,
            "provinces" => $provinces->data,
            "cities" => $cities->data
        ];
        return view('forms.seller_form', $data);
    }

    function update(Request $request){
        $params = [
            "user_id" => $request->id,
            "store_name" => $request->store_name,
            "balance" => $request->balance,
            "description" => $request->description,
            "phone_number" => $request->phone_number,
            "province_id" => $request->province_id,
            "city_id" => $request->city_id
        ];

        $response = Helper::privateApi(NetworkDao::updateSeller, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }

    function update_status(Request $request){
        $params = [
            "user_id" => $request->id,
            "action" => $request->action
        ];
        $response = Helper::privateApi(NetworkDao::updateUserStatus, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "store_name" => $request->search_query,
            "page" => $request->current_page
        ];

        $response = Helper::privateApi(NetworkDao::getSeller, $params, 'GET');
            $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/seller_item', ["sellers" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }
}
