<?php

namespace App\Http\Controllers;

use App\Constants\Message;
use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class SubCategoryController extends Controller
{
    private $paginate = 2;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getSubCategory, $params, 'GET');
        try {
            if ($response->success){
                $data = [
                    "sub_categories" => $response->data
                ];
                return view('sub_categories', $data);
            }else{
                $request->session()->now('response', $response);
                return view('sub_categories');
            }
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => Message::SERVER_ERROR  
            ];
            $request->session()->now('response', json_decode(json_encode($response)));
            return view('sub_categories');
        }
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "search" => $request->search_query,
            "page" => $request->current_page
        ];
        $response = Helper::privateApi(NetworkDao::getSubCategory, $params, 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/sub_category_item', ["sub_categories" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function create(Request $request){
        $uploadParams = [
            "body" => [],
            "images" => [
                "image" => $request->subcategory_image
            ]
        ];
        $uploadResp =  Helper::privateApi(NetworkDao::uploadSubCategoryImage, $uploadParams, 'UPLOAD');
        if ($uploadResp->success){
            $params = [
                "item_category_id" => $request->category_id,
                "item_sub_category_name" => $request->subcategory_name,
                "item_sub_category_image" => $uploadResp->data
            ];
            $response = Helper::privateApi(NetworkDao::createSubCategory, $params, 'POST');
            return redirect()->back()->with('response', $response);
        }else{
            return redirect()->back()->with('response', $uploadResp);
        }
    }

    function update(Request $request){
        $uploadParams = [
            "body" => [],
            "images" => []
        ];

        $image_url = $request->subcategory_old_image;
        if ($request->subcategory_image){
            $uploadParams['images'] = [
                "image" => $request->subcategory_image
            ]; 
            $uploadResp =  Helper::privateApi(NetworkDao::uploadSubCategoryImage, $uploadParams, 'UPLOAD');
            if ($uploadResp->success){
                $image_url = $uploadResp->data;
            }else{
                return redirect()->back()->with('response', $uploadResp);
            }
        }
        
        $params = [
            "item_sub_category_id" => $request->id,
            "item_category_id" => $request->category_id,
            "item_sub_category_name" => $request->subcategory_name,
            "item_sub_category_image" => $image_url,
        ];

        $response = Helper::privateApi(NetworkDao::updateSubCategory, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }

    function delete(Request $request){
        $params = [
            "item_sub_category_id" => $request->id,
        ];
        $response = Helper::privateApi(NetworkDao::deleteSubCategory, $params, 'DELETE');
        return redirect()->back()->with('response', $response);
    }

    function get_categories(){
        $params = [
            "paginate" => 100,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getCategory, $params, 'GET');
        return json_encode($response->data);
    }

    function get_all_subcategories(Request $request){
        $params = [
            "paginate" => 100,
            "item_category_id" => $request->category_id
        ];
        $response = Helper::privateApi(NetworkDao::getSubCategory, $params, 'GET');
        return json_encode($response->data);
    }
}
