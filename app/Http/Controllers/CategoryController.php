<?php

namespace App\Http\Controllers;

use App\Constants\Message;
use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class CategoryController extends Controller
{
    private $paginate = 10;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getCategory, $params, 'GET');
        try {
            if ($response->success){
                $data = [
                    "categories" => $response->data
                ];
                return view('categories', $data);
            }else{
                $request->session()->now('response', $response);
                return view('categories');
            }
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => Message::SERVER_ERROR  
            ];
            $request->session()->now('response', json_decode(json_encode($response)));
            return view('categories');
        }
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "search" => $request->search_query,
            "page" => $request->current_page
        ];
        $response = Helper::privateApi(NetworkDao::getCategory, $params, 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/category_item', ["categories" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function create(Request $request){
        $uploadParams = [
            "body" => [],
            "images" => [
                "image" => $request->category_image
            ]
        ];
        $uploadResp =  Helper::privateApi(NetworkDao::uploadCategoryImage, $uploadParams, 'UPLOAD');

        if ($uploadResp->success){
            $params = [
                "item_category_image" => $uploadResp->data,
                "item_category_name" => $request->category_name
            ];

            $response = Helper::privateApi(NetworkDao::createCategory, $params, 'POST');
            return redirect()->back()->with('response', $response);
        }else{
            return redirect()->back()->with('response', $uploadResp);
        }
    }

    function update(Request $request){
        $uploadParams = [
            "body" => [],
            "images" => []
        ];

        $image_url = $request->category_old_image;
        if ($request->category_image){
            $uploadParams['images'] = [
                "image" => $request->category_image
            ]; 
            $uploadResp =  Helper::privateApi(NetworkDao::uploadCategoryImage, $uploadParams, 'UPLOAD');
            if ($uploadResp->success){
                $image_url = $uploadResp->data;
            }else{
                return redirect()->back()->with('response', $uploadResp);
            }
        }
        
        $params = [
            "item_category_id" => $request->id,
            "item_category_image" =>  $image_url,
            "item_category_name" => $request->category_name
        ];

        $response = Helper::privateApi(NetworkDao::updateCategory, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }

    function delete(Request $request){
        $params = [
            "item_category_id" => $request->id,
        ];
        $response = Helper::privateApi(NetworkDao::deleteCategory, $params, 'DELETE');
        return redirect()->back()->with('response', $response);
    }
}
