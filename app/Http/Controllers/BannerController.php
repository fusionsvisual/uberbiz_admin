<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class BannerController extends Controller
{
    function index(Request $request){
        $params = [];
        $response = Helper::privateApi(NetworkDao::getBanner, $params, 'GET');
        $data = [
            "banners" => $response->data
        ];
        return view('banners', $data);
    }

    function filter(Request $request){
        // $params = [
        //     "paginate" => 1,
        //     // "username" => $request->username,
        //     "page" => $request->current_page
        // ];
        // $response = Helper::privateApi(NetworkDao::getCategory, $params, 'GET');
        // $data = [
        //     "pagination" => view('templates/pagination', [
        //         'total_page' => ceil($response->data->total/ $response->data->per_page), 
        //         'total_per_page' => $response->data->per_page,
        //         'current_page' => $response->data->current_page
        //     ])->render(),
        //     "items" => view('items/category_item', ["categories" => $response->data])->render(),
        //     "total_page" => ceil($response->data->total/ $response->data->per_page)
        // ];
        // return $data;
    }

    function create(Request $request){
        echo $request->banner;
        $params = [
            "body" => [],
            "images" => [
                "image" => $request->banner
            ]
        ];
        $response = Helper::privateApi(NetworkDao::uploadBanner, $params, 'UPLOAD');
        return redirect()->back()->with('response', $response);
    }

    function update(Request $request){
        // if($request->method() == "POST"){
        //     $params = [
        //         "user_id" => $request->id,
        //         "username" => $request->username,
        //         "birthdate" => $request->birthdate,
        //         "email" => $request->email,
        //         "phone_number" => $request->phone_number,
        //         "gender" => $request->gender,
        //     ];
        //     $response = Helper::privateApi(NetworkDao::updateUser, $params, 'PATCH');
        //     return redirect()->back()->with('response', $response);
        // }else{
        //     $params = [
        //         "user_id" => $request->id
        //     ];
        //     $response = Helper::privateApi(NetworkDao::getUser, $params, 'GET');
        //     $data = [
        //         "buyer" => $response->data
        //     ];
        //     return view('forms.buyer_form', $data);
        // }
    }

    function delete(Request $request){
        $params = [
            "banner_id" => $request->id,
        ];
        $response = Helper::privateApi(NetworkDao::deleteBanner, $params, 'DELETE');
        return redirect()->back()->with('response', $response);
    }
}
