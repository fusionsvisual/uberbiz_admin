<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Echo_;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class TransactionController extends Controller
{
    private $paginate = 10;
    
    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "start_date" => date('Y-m-01'),
            "end_date" => date("Y-m-t"),
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getTransaction, $params, 'GET');
        if ($response){
            if ($response->success){
                $data = [
                    "histories" => $response->data
                ];
                return view('transaction_history_list', $data);
            }else{
                echo $response->message;
            }
        }else{
            echo "null";
        }
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "page" => $request->current_page,
            "search" => $request->search_query
        ];
        $response = Helper::privateApi(NetworkDao::getTransaction, $params, 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/transaction_history_item', ["histories" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function detail(Request $request){
        $params = [
            "order_id" => $request->id
        ];
        $detail = Helper::privateApi(NetworkDao::getTransaction, $params, 'GET');
        // echo json_encode($detail);
        $data = [
            "transaction" => $detail->data
        ];
        return view('forms.transaction_form', $data);
    }

    function update(Request $request){
        $params = [
            "order_id" => $request->id,
            "action" => $request->action
        ];
        echo json_encode($params);
        // $response = Helper::privateApi(NetworkDao::updateTransaction, $params, 'PATCH');
        // return redirect()->back()->with('response', $response);
    }
}
