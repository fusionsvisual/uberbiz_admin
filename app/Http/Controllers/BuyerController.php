<?php

namespace App\Http\Controllers;

use App\Constants\Message;
use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class BuyerController extends Controller
{
    private $paginate = 10;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getUser, $params, 'GET');

        try {
            if ($response->success){
                $data = [
                    "buyers" => $response->data
                ];
                return view('buyer_list', $data);
            }else{
                $request->session()->now('response', $response);
                return view('buyer_list');
            }
            
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()    
            ];
            $request->session()->now('response', json_decode(json_encode($response)));
            return view('buyer_list');
        }
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "username" => $request->search_query,
            "page" => $request->current_page
        ];

        $response = Helper::privateApi(NetworkDao::getUser, $params, 'GET');
            $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/buyer_item', ["buyers" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function detail(Request $request){
        $params = [
            "user_id" => $request->id
        ];
        $response = Helper::privateApi(NetworkDao::getUser, $params, 'GET');
        $data = [
            "buyer" => $response->data
        ];
        return view('forms.buyer_form', $data);
    }

    function update(Request $request){
        $params = [
            "user_id" => $request->id,
            "username" => $request->username,
            "date_of_birth" => date_format(date_create($request->birthdate),"Y-m-d"),
            "email" => $request->email,
            "phone_number" => $request->phone_number,
            "gender" => $request->gender,
        ];

        echo $request->birthdate;

        if($request->birthdate){
            echo "ABC";
        }else{
            echo "BCA";
        }

        // $response = Helper::privateApi(NetworkDao::updateUser, $params, 'PATCH');
        // if (!$response){
        //     $response['success'] = false;
        //     $response['message'] = Message::SERVER_ERROR;
        //     $response = (object) $response;
        // }
        // return redirect()->back()->with('response', $response);
    }

    function update_status(Request $request){
        $params = [
            "user_id" => $request->id,
            "action" => $request->action
        ];
        $response = Helper::privateApi(NetworkDao::updateUserStatus, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }

    function delete(Request $request){
        $params = [
            "item_category_id" => $request->id,
        ];
        $response = Helper::privateApi(NetworkDao::deleteCategory, $params, 'DELETE');
        return redirect()->back()->with('response', $response);
    }
}
