<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class UserController extends Controller
{
    private $paginate = 10;

    function index(Request $request){
        if ($request->session()->has('user')){
            return redirect('/transactions');
        }else{
            return redirect('login');
        }
    }

    function login(Request $request){
        if ($request->method() == "POST"){
            $params = [
                "username" => $request->username,
                "password" => $request->password
            ];
            $response = Helper::publicApi('admin/login', $params, 'POST');
            if ($response->success){
                $request->session()->put('user.token', $response->data->token);

                $user_detail = Helper::privateApi('admin/profile/details', [], 'GET');
                $request->session()->put('user.data', $user_detail->data);
                return redirect('/transactions');
            }else{
                return redirect()->back()->with("response", $response);
            }
        }else{
            return view('login');
        }
    }

    function logout(Request $request){
        $request->session()->forget('user');
        return redirect('login');
    }

    function transaction_read(Request $request){
        $params = [
            "paginate" => 1,
            "page" => 7
        ];
        $response = Helper::privateApi(NetworkDao::getTransaction, $params, 'GET');
        $data = [];
        if ($response->success){
            $data["histories"] = $response->data;
        }else{
            return redirect('transactions')->with('response', $response);
        }
        return view('transaction_history_list', $data);
    }

    function transaction_detail(Request $request){
        $params = [
            "order_id" => $request->id
        ];
        $response = Helper::privateApi(NetworkDao::getTransaction, $params, 'GET');
        $data = [
            "transaction" => $response
        ];
    }

    function pagination_update(Request $request){
        $data = [
            'total_page' => $request->total_page, 
            'total_per_page' => $request->total_per_page,
            'current_page' => $request->current_page
        ];
        return view('templates/pagination', $data);
    }
    
    function seller_read(Request $request){
        $params = [
            "paginate" => 10,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getSeller, $params, 'GET');
        echo json_encode($response);
        $data = [
            "sellers" => $response->data
        ];
        return view('seller_list', $data);
    }

    function request_quotation_read(Request $request){
        return view('request_quotation_list');
    }

    function quotation_read(Request $request){
        return view('quotation_list');
    }

    function product_read(Request $request){
        return view('product_list');
    }
}
