<?php

namespace App\Http\Controllers;

use App\Constants\Message;
use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ProductController extends Controller
{
    private $paginate = 10;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getProduct, $params, 'GET');
        try {
            if ($response->success){
                $data = [
                    "products" => $response->data
                ];
                return view('products', $data);
            }else{
                $request->session()->now('response', $response);
                return view('products');
            }
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'message' => Message::SERVER_ERROR  
            ];
            $request->session()->now('response', json_decode(json_encode($response)));
            return view('products');
        }
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "search" => $request->search_query,
            "page" => $request->current_page
        ];
        $response = Helper::privateApi(NetworkDao::getProduct, $params, 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/product_item', ["products" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function detail(Request $request){
        $params = [
            "item_id" => $request->id
        ];
        $response = Helper::privateApi(NetworkDao::getProduct, $params, 'GET');
        $categories = Helper::privateApi(NetworkDao::getCategory, [
            "paginate" => 100
        ], 'GET');

        $subcategories_request = new Request();
        $subcategories_request->category_id = $response->data->item_sub_category->item_category_id;
        $subcategories = json_decode((new SubCategoryController)->get_all_subcategories($subcategories_request));

        $units = (new UnitController)->get_all_units();
        $data = [
            "product" => $response->data,
            "units" => $units->data,
            "categories" => $categories->data,
            "subcategories" => $subcategories->data
        ];
        return view('forms.product_form', $data);
    }

    function create(){
        echo "CREATE";
    }

    function update(Request $request){

        

        $params = [
            "item_id" => $request->id,
            "item_name" => $request->name,
            "item_description" => $request->description,
            "price" => $request->price,
            "minimum_order" => $request->minimum_order,
            "unit_id" => $request->unit_id,
            "item_sub_category_id" => $request->subcategory_id,
            "image" => $request->image
        ];
        echo json_encode($params);

        // $response = Helper::privateApi(NetworkDao::updateProduct, $params, 'PATCH');
        // return redirect()->back()->with('response', $response);
    }

    function delete(Request $request){
        $params = [
            "item_id" => $request->id,
        ];
        $response = Helper::privateApi(NetworkDao::deleteProduct, $params, 'DELETE');
        return redirect()->back()->with('response', $response);
    }

    function upload_image(Request $request){
        $params = [
            "body" => [],
            "images" => [
                "image" => $request->image
            ]
        ];
        $response = Helper::privateApi(NetworkDao::uploadProductImage, $params, 'UPLOAD');
        return json_encode($response);
    }

    function update_image_local(Request $request){
        $data = [
            "image_item" => view('product_form/items/product_image_item', ["index" => $request->index, "src" => $request->src])->render(),
            "image_placeholder_item" => view('product_form/items/product_image_placeholder_item', ["index" => $request->index + 1])->render()
        ];
        return $data;
    }

    function delete_image_local(Request $request){
        return view('product_form/items/product_image_placeholder_item', ["index" => $request->index])->render();
    }

    function get_subcategories(){
        $params = [
            "paginate" => 100,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getSubCategory, $params, 'GET');
        return json_encode($response->data);
    }
}
