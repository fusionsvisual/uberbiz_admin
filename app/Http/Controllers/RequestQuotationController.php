<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestQuotationController extends Controller
{
    private $paginate = 10;

    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getRequestQuotation, $params, 'GET');
        // echo json_encode($response);
        try {
            if ($response->success){
                $data = [
                    "requests" => $response->data
                ];
                return view('request_quotation_list', $data);
            }else{
                // $request->session()->now('response', $response);
                // return view('seller_list');
            }
            
        } catch (Exception $e) {
            echo $e->getMessage();
            // $response = [
            //     'success' => false,
            //     'message' => $e->getMessage()    
            // ];
            // $request->session()->now('response', json_decode(json_encode($response)));
            // return view('seller_list');
        }
        // return view('request_quotation_list', $data);
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "search" => $request->search_query,
            "page" => $request->current_page
        ];

        $response = Helper::privateApi(NetworkDao::getRequestQuotation, $params, 'GET');
            $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('items/request_quotation_item', ["requests" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function delete(Request $request){
        echo "DELETE";
    }
}
