<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class StoreVerificationController extends Controller
{
    private $paginate = 10;

    // function get_all_units(){
    //     $params = [
    //         "paginate" => 100
    //     ];
    //     $response = Helper::privateApi(NetworkDao::getAllUnit, $params, 'GET');
    //     return $response;
    // }
    
    function index(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi(NetworkDao::getAllStoreVerfication, $params, 'GET');

        $data = [
            "verifications" => $response->data
        ];
        return view('store_verification.store_verification_list', $data);
    }

    function filter(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => $request->current_page
        ];
        $response = Helper::privateApi(NetworkDao::getAllStoreVerfication, $params, 'GET');
        $data = [
            "pagination" => view('templates/pagination', [
                'total_page' => ceil($response->data->total/ $response->data->per_page), 
                'total_per_page' => $response->data->per_page,
                'current_page' => $response->data->current_page
            ])->render(),
            "items" => view('store_verification.items.store_verification_item', ["verifications" => $response->data])->render(),
            "total_page" => ceil($response->data->total/ $response->data->per_page)
        ];
        return $data;
    }

    function update(Request $request){
        $params = [
            "seller_document_id" => $request->id,
            "action" => $request->action
        ];
        $response = Helper::privateApi(NetworkDao::updateStoreVerfication, $params, 'PATCH');
        return redirect()->back()->with('response', $response);
    }
}
