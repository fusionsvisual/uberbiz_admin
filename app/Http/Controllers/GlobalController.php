<?php

namespace App\Http\Controllers;

use App\Constants\Network;
use App\Constants\NetworkDao;
use App\Helpers\Helper;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class GlobalController extends Controller
{

    function get_all_cities(Request $request){
        $cities = Helper::privateApi(NetworkDao::getCities, ["province_id" => $request->province_id], 'GET');
        return json_encode($cities->data);
    }

}
