<?php

namespace App\Helpers;

use App\Constants\Network;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class QBHelper{

    private $base_url = "https://api.quickblox.com/";

    function privateApi($url, $params, $method){
        switch ($method) {
            case 'POST':
                $url = $this->base_url.$url;
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'QB-Token' => session('user')['qb_token']
                ])->post($url, $params);
                return json_decode($response);
                break;
            case 'GET':
                $url = $this->base_url.$url;
                $response = Http::withHeaders([
                    'QB-Token' => session('user')['qb_token']
                ])->get($url, $params);
                return json_decode($response);
                break;
            case 'PATCH':
                $url = Network::get_base_url().$url;
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'QB-Token' => session('user')['qb_token']
                ])->patch($url, $params);
                return json_decode($response);
                break;
            case 'DELETE':
                $url = $this->base_url.$url;
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'QB-Token' => session('user')['qb_token']
                ])->delete($url, $params);
                return json_decode($response);
                break;
            default:
                break;
        }
    }

    function publicApi($url, $params, $method){
        switch ($method) {
            case 'POST':
                $url = $this->base_url.$url;
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                ])->post($url, $params);
                return json_decode($response);
                break;
            case 'GET':
                $url = Network::get_base_url().$url;
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                ])->get($url, $params);
                return json_decode($response);
                break;
            case 'UPLOAD':
                $response = Http::attach(
                    'image_url', $params['image'], "abc.jpg"
                )->post($url, $params['body']);
                return $response;
                break;
            default:
                break;
        }
    }
}

?>
