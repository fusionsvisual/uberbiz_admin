<?php

namespace App\Constants;

class NetworkDao {

    // GLOBAL
    const getProvinces = 'province_list';
    const getCities = 'city_list';

    // TRANSACTION
    const getTransaction = 'admin/order/read';
    const updateTransaction = 'admin/order/update';

    // USER
    const getUser = 'admin/user/admin_read_user';
    const getSeller = 'admin/user/admin_read_store';
    const updateUser = 'admin/user/update_user';
    const updateSeller = 'admin/user/update_store';
    const updateUserStatus = 'admin/user/ban_unban_user';

    // CATEGORY
    const getCategory = 'admin/item_category/read';
    const uploadCategoryImage = 'admin/item_category/upload';
    const createCategory = 'admin/item_category/create';
    const updateCategory = 'admin/item_category/update';
    const deleteCategory = 'admin/item_category/delete';

    // SUBCATEGORY
    const getSubCategory = 'admin/item_sub_category/read';
    const uploadSubCategoryImage = 'admin/item_sub_category/upload';
    const createSubCategory = 'admin/item_sub_category/create';
    const updateSubCategory = 'admin/item_sub_category/update';
    const deleteSubCategory = 'admin/item_sub_category/delete';

    // REQUEST QUOTATION
    const getRequestQuotation = 'admin/request_quotation/read';

    // PRODUCT
    const getProduct = 'admin/item/read';
    const uploadProductImage = 'admin/item_image/upload';
    const updateProduct = 'admin/item/update';
    const deleteProduct = 'admin/item/delete';

    // WITHDRAW
    const getWithdraw = 'admin/withdraw/read';
    const updateWithdraw = 'admin/withdraw/update';

    // BANNER
    const getBanner = 'admin/banner/read';
    const deleteBanner = 'admin/banner/delete';
    const uploadBanner = 'admin/banner/upload';

    // UNIT
    const getAllUnit = 'admin/unit/read';

    // STORE VERIFICATION
    const getAllStoreVerfication = 'admin/seller_document/read';
    const updateStoreVerfication = 'admin/seller_document/update';
}

?>