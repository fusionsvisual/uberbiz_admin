<?php

namespace App\Constants;

class Network {
   static function get_base_url(){
      if (env('APP_ENV') == "local" || env('APP_ENV') == "development"){
         return "https://api-uberbiz.fusionsvisual.id/api/";
      }else{
         return "https://api-uberbiz.fusionsvisual.id/api/";
      }
   }

   static function get_asset_url(){
      if (env('APP_ENV') == "local" || env('APP_ENV') == "development"){
         return "https://api-uberbiz.fusionsvisual.id/";
      }else{
         return "https://api-uberbiz.fusionsvisual.id/";
      }
   }
}

?>