@extends('layouts.sidebar')

@section('wrapper-content')
    <section>
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif
            
            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" name="store_name" placeholder="Search by store name">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Store Name</th>
                            <th scope="col">Rating</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @include('items.seller_item', ["sellers" => $sellers])
                        </tbody>
                        @include('items.shimmers.shimmer_sellers_item')
                    </table>
                </div>
            </div>
            <div class="col-12">
                <div class="container-pagination">
                    @if (isset($sellers))
                        @include('templates/pagination', [
                            'total_page' => ceil($sellers->total/ $sellers->per_page), 
                            'total_per_page' => $sellers->per_page,
                            'current_page' => $sellers->current_page
                        ])
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('wrapper-script')
    <script>
        $url = "{{ url('sellers/filter') }}"
        $totalPage = "{{ ceil($sellers->total/ $sellers->per_page) }}"
        $totalPerPage = "{{ $sellers->per_page }}"
        $currentPage = "{{ $sellers->current_page }}"

        setupSearchListener();
        setupPagination();
    </script>
@endsection