@extends('layouts.sidebar')

@section('wrapper-content')
    <section>
        <div class="popup-container d-none">
            <div class="popup-overlay"></div>
            <div class="popup-content-container">
                <div class="popup-content" style="width: 500px">
                    <div class="page-content-inner">
                        <form id="popup-form" action="{{ url('categories/create') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="d-flex flex-column">
                                <h2 id="popup-title" class="font-semiBold mx-auto mb-3">Create Subcategory</h2>
                                <div class="mx-auto mb-3" style="width:200px; height:200px;">
                                    <div class="d-square-container d-img-placeholder">
                                        <img class="img-preview d-none" src="" alt="">
                                        <div class="d-square-content d-flex">
                                            <div class="placeholder-img-container d-flex flex-column m-auto">
                                                <img class="mx-auto mb-3" src="{{ asset('images/ic_upload_image.svg') }}" alt="" style="width: 45px; height: 45px;">
                                                <p class="placeholder-title" style="font-size: 0.75rem">Upload image (300x300)</p>
                                            </div>
                                            <div class="placeholder-input-container">
                                                <label for="input-img"></label>
                                                <input id="input-img" type="file" name="subcategory_image" accept="image/jpeg, image/png" required>
                                                <input type="hidden" name="subcategory_old_image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <p class="mb-3">Select Category</p>
                                    <select id="category_name" class="custom-select" name="category_id" required>
                                    </select>
                                </div>
                                <p class="mb-3">Subcategory Name</p>
                                <input class="form-control mb-4" type="text" name="subcategory_name" placeholder="ex. Pakaian" required>
                                <div class="row">
                                    <div class="col-12 col-xl-6">
                                        <button id="popup-btn-close" class="btn btn-outline-primary w-100">Cancel</button>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <button id="popup-btn-save" class="btn btn-primary w-100">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif

            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" name="name" placeholder="Search by subname">
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 mb-4">
                <div class="d-flex">
                    <button id="list-btn-create" class="btn btn-done ml-auto" href="{{ url('categories/create') }}">
                        + Create Subcategory
                    </button>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table align-top">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Parent Name</th>
                            <th scope="col">Sub Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @isset($sub_categories)
                                @include('items.sub_category_item', ["sub_categories" => $sub_categories])
                            @endisset
                            @include('items.shimmers.shimmer_subcategories_item')
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12">
                <div class="container-pagination">
                    @isset($sub_categories)
                        @include('templates/pagination', [
                            'total_page' => ceil($sub_categories->total/ $sub_categories->per_page), 
                            'total_per_page' => $sub_categories->per_page,
                            'current_page' => $sub_categories->current_page
                        ])  
                    @endisset
                </div>
            </div>
        </div>
    </section>
@endsection

@section('wrapper-script')
    @isset($sub_categories)
        <script>
            $url = "{{ url('sub_categories/filter') }}"
            $totalPage = "{{ ceil($sub_categories->total/ $sub_categories->per_page) }}"
            $totalPerPage = "{{ $sub_categories->per_page }}"
            $currentPage = "{{ $sub_categories->current_page }}"

            setupSearchListener();
            setupPagination();
        </script>
    @endisset

    <script>
        $category_ajax = null
        $current_state = "CREATE"

        $('#popup-btn-save').click(function (e) { 
            if ($('input[name="subcategory_image"]').val()){
                $('#popup-form').submit();
            }else{
                if ($('input[name="subcategory_old_image"]').val()){
                    $('input[name="subcategory_image"]').removeAttr('required');
                }
            }
        });

        $(document).ready(function () {
            setupEditButton();
        });

        $('#list-btn-create').click(function (e) { 
            e.preventDefault();

            // Setup parent category list
            setupCategorySelect();
            
            $current_state = "CREATE"
            $('#popup-title').text("Create Subcategory");
            $('#popup-form').attr('action', "{{ url('sub_categories/create') }}");
            $('.popup-container').removeClass('d-none');
        });

        function setupEditButton(){
            $('.list-btn-edit').click(function (e) { 
                e.preventDefault();

                // Setup parent category list
                $category_id = $(this).data('category-id');
                setupCategorySelect($category_id);

                $subcategory_id = $(this).data('id');
                $index = $(this).data('idx');
                $subcategory_name = $('.subcategory_name').eq($index).text();
                $image = $('input[name="image_url"]').eq($index).val();

                if ($image){
                    $('input[name="subcategory_old_image"]').val($image);
                    $('.popup-container .img-preview').removeClass('d-none');
                    $('.placeholder-img-container').removeClass('d-flex');      
                    $('.placeholder-img-container').addClass('d-none');
                    $('.popup-container .img-preview').attr('src', "{{ Network::get_asset_url() }}" + $image);
                }
                
                // Setup category name value
                $('input[name="subcategory_name"]').val($subcategory_name);

                $current_state = "EDIT"
                $('#popup-title').text("Edit Category");
                $form_url = "sub_categories/" + "update/" + $subcategory_id;
                $('#popup-form').attr('action', $form_url);
                $('.popup-container').removeClass('d-none');
            });
        }

        function setupCategorySelect($category_id){
            $category_ajax = $.ajax({
                type: "GET",
                url: "{{ url('sub_categories/get_categories') }}",
                data: $params,
                dataType: "text",
                success: function (response) {
                    $json = JSON.parse(response);
                    $.each($json.data, function (idx, val) { 
                        $option = $("<option>", {value: val.id, text: val.item_category_name});
                        $('select[name="category_id"]').append($option)
                        
                        if ($category_id == val.id){
                            console.log(val.id);
                            $('select[name="category_id"]').val(val.id).change();
                        }
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        }

        $('#input-img').change(function (e) { 
            var oFReader = new FileReader();
            oFReader.readAsDataURL($(this)[0].files[0]);
            $paymentReceiptImage = $(this)[0].files[0]

            oFReader.onload = function (oFREvent) {
                $('.img-preview').removeClass('d-none');
                $('.placeholder-img-container').removeClass('d-flex');
                $('.placeholder-img-container').addClass('d-none');
                $('.popup-container .img-preview').attr('src', oFREvent.target.result);
            };
        });

        $('#popup-btn-close').click(function (e) { 
            e.preventDefault();
            // Refresh popup content
            $('select[name="category_id"]').empty();
            $('input[name="subcategory_name"]').val("");
            $('input[name="subcategory_image"]').val("");
            $('input[name="subcategory_old_image"]').val("");
            $('.popup-container .img-preview').addClass('d-none');
            $('.placeholder-img-container').addClass('d-flex');      
            $('.placeholder-img-container').removeClass('d-none');
            $('.popup-container .img-preview').attr('src', "");

            // Abort category list ajax
            $category_ajax.abort();

            // Hide overlay
            $('.popup-container').addClass('d-none');
        });

        function didFilterDataFinish(){
            console.log("ABC");
            setupEditButton();
        }
    </script>
@endsection