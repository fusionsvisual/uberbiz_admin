@extends('layouts.sidebar')

@section('wrapper-content')
    <section>
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif

            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" placeholder="Search by name">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Store Name</th>
                            <th scope="col">Total Sold</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @isset($products)
                                @include('items.product_item', ["products" => $products])
                            @endisset
                            @include('items.shimmers.shimmer_products_item')
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="col-12">
                <div class="container-pagination">
                    @isset($products)
                        @include('templates/pagination', [
                            'total_page' => ceil($products->total/ $products->per_page), 
                            'total_per_page' => $products->per_page,
                            'current_page' => $products->current_page
                        ])
                    @endisset
                </div>
            </div>
        </div>
    </section>
@endsection

@section('wrapper-script')
    @isset($products)
        <script>
            $url = "{{ url('products/filter') }}"
            $totalPage = "{{ ceil($products->total/ $products->per_page) }}"
            $totalPerPage = "{{ $products->per_page }}"
            $currentPage = "{{ $products->current_page }}"

            setupSearchListener();
            setupPagination();
        </script>
    @endisset
@endsection