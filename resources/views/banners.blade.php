@extends('layouts.sidebar')
@section('wrapper-content')
    <section id="section-banner">
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif
            
            <div class="col-12">
                <div class="mb-5" style="background: white">
                    <div class="header-form" style="border-bottom:">
                        <p class="header-title">Promotional Banners</p>
                    </div>
                    <div class="content-form">
                        <p class="text-muted mb-4">Upload up to 5 promotional banners for seasonal events </p>
                        <div class="row">
                            @for ($i = 0; $i < 5; $i++)
                                <div class="col-12 col-xl-4 mb-4">
                                    <form action="{{ url('banners/create') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="banner-d mb-3">
                                            <img class="banner-img" src="@if($i < count($banners)) {{ Network::get_asset_url().$banners[$i]->image }} @else {{ asset('images/img_banner_placeholder.png') }} @endif" alt="">
                                            @if($i < count($banners))
                                                <div class="banner-overlay @if($i >= count($banners)) d-none @endif">
                                                    <div class="banner-menu-d">
                                                        <div class="m-auto">
                                                            {{-- <button class="btn btn-primary d-block mb-2">
                                                                Change
                                                            </button> --}}
                                                            <a class="btn btn-danger" href="{{ url('banners/delete/'.$banners[$i]->id) }}">
                                                                Remove
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <input id="banner-input-file" type="file" name="banner" accept="image/png, image/jpeg" id="">
                                                <label for="banner-input-file"></label>
                                            @endif
                                        </div>
                                        <p class="banner-title text-center">Banner {{ $i + 1 }}</p>
                                    </form>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
@endsection

@section('wrapper-script')

<script>
    $('#banner-input-file').change(function (e) { 
        $(this).parent().parent().submit();
    });
</script>
    
@endsection