@extends('layouts.sidebar')

@section('wrapper-content')
    <section>
        <div class="row">
            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="" type="text" name="" placeholder="Search by name">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Date</th>
                            <th scope="col">Title</th>
                            <th scope="col">Created By</th>
                            <th scope="col">Received By</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                          <tr>
                            <td>1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td>@mdo</td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection