@foreach($verifications->data as $idx => $verification)
    <tr>
        <td>{{ (($verifications->current_page - 1) * $verifications->per_page) + ($idx + 1) }}</th>
        <td>{{ $verification->seller_user->store_name }}</td>
        <td>
            <button class="btn btn-text" data-url="{{ $verification->npwp }}" onclick="seeNpwpAction(this)">
                <div class="d-flex">
                    <img class="my-auto mr-1" src="{{ asset('images/ic_attachment.svg') }}" alt="" width="18" height="18">
                    <span>See NPWP</span>
                </div>
            </button>
        </td>
        <td>
            <button class="btn btn-text" data-url="{{ $verification->deed }}" onclick="seeDeedAction(this)">
                <div class="d-flex">
                    <img class="my-auto mr-1" src="{{ asset('images/ic_attachment.svg') }}" alt="" width="18" height="18">
                    <span>See Deed</span>
                </div>
            </button>
        </td>
        <td>{{ $verification->status->status_name }}</td>
        <td class="column-action">
            <a class="btn btn-done" href="{{ url('store_verification/'.$verification->id.'/accept') }}">
                Confirm
            </a>
            <a class="btn btn-outline-danger" href="{{ url('store_verification/'.$verification->id.'/reject') }}">
                Reject
            </a>
        </td>
    </tr>
@endforeach
