@extends('layouts.sidebar')

@section('wrapper-content')

    <div class="popup-container d-none">
        <div class="popup-overlay"></div>
        <div class="popup-content-container">
            <div class="popup-content" style="background: transparent;">
                <div class="page-content-inner" style="background: transparent">
                    <img id="documentImage" src="" alt="" width="500px"  style="object-fit: contain">
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif

            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" name="" placeholder="Search by name">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Store Name</th>
                            <th scope="col">NPWP</th>
                            <th scope="col">Company Deed</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @include('store_verification.items.store_verification_item', [
                                "verifications" => $verifications
                            ])
                        </tbody>
                        @include('store_verification.items.shimmer.shimmer_store_verification_item')
                    </table>
                </div>
            </div>
            <div class="col-12">
                <div class="container-pagination">
                    @include('templates/pagination', [
                        'total_page' => ceil($verifications->total/ $verifications->per_page), 
                        'total_per_page' => $verifications->per_page,
                        'current_page' => $verifications->current_page
                    ])
                </div>
            </div>
        </div>
    </section>
@endsection

@section('wrapper-script')
    <script>

        $url = "{{ url('store_verification/filter') }}"
        $totalPage = "{{ ceil($verifications->total/ $verifications->per_page) }}"
        $totalPerPage = "{{ $verifications->per_page }}"
        $currentPage = "{{ $verifications->current_page }}"

        setupSearchListener()
        setupPagination();

        $('.popup-overlay').click(function (e) { 
            $('.popup-container').addClass('d-none');
        });

        function seeNpwpAction(e){
            $image_url = $(e).data('url');

            $('.popup-container').removeClass('d-none');
            $('#documentImage').attr('src', "{{ Network::get_asset_url() }}" + $image_url)
        }

        function seeDeedAction(e){
            $image_url = $(e).data('url');

            $('.popup-container').removeClass('d-none');
            $('#documentImage').attr('src', "{{ Network::get_asset_url() }}" + $image_url)
        }
    </script>
@endsection