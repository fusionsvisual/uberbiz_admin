@extends('layouts.sidebar')

@section('wrapper-content')
    <section>
        <div class="row">
            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" placeholder="Search by sender name">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Date</th>
                            <th scope="col">Title</th>
                            <th scope="col">Category</th>
                            <th scope="col">Sender</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @include('items.request_quotation_item', ["requests" => $requests])
                        </tbody>
                        @include('items.shimmers.shimmer_requests_item')
                    </table>
                </div>
            </div>
            <div class="col-12">
                <div class="container-pagination">
                    @include('templates/pagination', [
                        'total_page' => ceil($requests->total/ $requests->per_page), 
                        'total_per_page' => $requests->per_page,
                        'current_page' => $requests->current_page
                    ])
                </div>
            </div>
        </div>
    </section>
@endsection

@section('wrapper-script')
    <script>
        $url = "{{ url('request_quotations/filter') }}"
        $totalPage = "{{ ceil($requests->total/ $requests->per_page) }}"
        $totalPerPage = "{{ $requests->per_page }}"
        $currentPage = "{{ $requests->current_page }}"

        setupSearchListener();
        setupPagination();
    </script>
@endsection