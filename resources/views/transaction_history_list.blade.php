@extends('layouts.sidebar')

@section('wrapper-content')
    <section>
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif

            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" name="" placeholder="Search by invoice">
                    </div>
                </div>
            </div>

            <div class="col-6 col-lg-3 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <input class="" type="text" name="start_date" placeholder="Start Date" readonly>
                        <img src="{{ asset('images/ic_chevron_down.svg') }}" alt="" width="16px">
                    </div>
                </div>
            </div>

            <div class="col-6 col-lg-3 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <input class="" type="text" name="end_date" placeholder="End Date " readonly>
                        <img src="{{ asset('images/ic_chevron_down.svg') }}" alt="" width="16px">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Date</th>
                            <th scope="col">Invoice ID</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @include('items.transaction_history_item', ["histories" => $histories])
                        </tbody>
                        @include('items.shimmers.shimmer_transaction_item')
                    </table>
                </div>
            </div>

            <div class="col-12">
                <div class="container-pagination">
                    @include('templates/pagination', [
                        'total_page' => ceil($histories->total/ $histories->per_page), 
                        'total_per_page' => $histories->per_page,
                        'current_page' => $histories->current_page
                    ])
                </div>
            </div>
        </div>
    </section>
@endsection

@section('wrapper-script')
    <script>
        $url = "{{ url('transactions/filter') }}"
        $totalPage = "{{ ceil($histories->total/ $histories->per_page) }}"
        $totalPerPage = "{{ $histories->per_page }}"
        $currentPage = "{{ $histories->current_page }}"

        setupSearchListener();
        setupPagination();
        setupDatePickerFilter();
    </script>
@endsection