<div class="d-flex">
    <div class="pagination d-flex mx-auto ml-lg-auto mr-lg-0 py-2 px-2">
        <button class="btn btn-prev" data-page="{{ $current_page - 1 }}" @if($current_page == 1) disabled @endif>
            <div class="d-flex">
                <img class="my-auto mr-2" src="@if($current_page == 1) {{ asset('images/ic_prev.svg') }} @else {{ asset('images/ic_prev_active.svg') }} @endif" alt="">
                <span>Prev</span>
            </div>
        </button>
        <div class="d-none d-lg-block">
            @if($total_page < 6)
                @if ($total_page > 0)
                    @for($i = 1; $i <= $total_page; $i++)
                        <button class="btn btn-page @if($current_page == $i) active @endif" data-page="{{ $i }}" @if($total_page == 1) disabled @endif>
                            {{ $i }}
                        </button>
                    @endfor
                @else
                    <button class="btn btn-page active" data-page="1" disabled>1</button>
                @endif
                
            @else
                @if($current_page < 4)
                    @for($i = 1; $i <= 4; $i++)
                        <button class="btn btn-page @if($current_page == $i) active @endif" data-page="{{ $i }}">{{ $i }}</button>
                    @endfor
                    <span>..</span>
                    <button class="btn btn-page" data-page="{{ $total_page }}">{{ $total_page }}</button>
                @elseif($current_page > ($total_page - 3))
                    <button class="btn btn-page" data-page="1">1</button>
                    <span>..</span>
                    @for($i = $total_page - 3; $i <= $total_page; $i++)
                        <button class="btn btn-page @if($current_page == $i) active @endif" data-page="{{ $i }}">{{ $i }}</button>
                    @endfor
                @else
                    <button class="btn btn-page" data-page="1">1</button>
                    <span>..</span>
                    @for($i = $current_page - 1; $i <= $current_page + 1; $i++)
                        <button class="btn btn-page @if($current_page == $i) active @endif" data-page="{{ $i }}">{{ $i }}</button>
                    @endfor
                    <span>..</span>
                    <button class="btn btn-page" data-page="{{ $total_page }}">{{ $total_page }}</button>
                @endif
            @endif
        </div>
        <div class="d-block d-lg-none">
            <span>{{ $current_page }}/{{ $total_page }}</span>
        </div>

        <button class="btn btn-next" data-page="{{ $current_page + 1 }}" @if($current_page == $total_page || $total_page == 0) disabled @endif>
            <div class="d-flex">
                <span class="mr-2">Next</span>
                <img class="my-auto" src="@if($current_page == $total_page || $total_page == 0) {{ asset('images/ic_next.svg') }} @else {{ asset('images/ic_next_active.svg') }} @endif" alt="">
            </div>
        </button>
    </div>
</div>
