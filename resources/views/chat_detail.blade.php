@extends('layouts.sidebar')

@section('wrapper-content')
<section>
    {{-- {{  json_encode($dialog) }} --}}
    <div class="row">
        <div class="col-12 mb-5">
            <a class="btn btn-primary" href="{{ url('chats') }}">
                <i class="fas fa-chevron-left"></i>
                Back
            </a>
        </div>
        <div class="col-12 mb-5">
            <h1 class="font-semiBold mb-3">Message Histories</h1>
            <p class="text-muted"> Buyer: <span class="font-semiBold">
                @isset ($dialog[0]->occupants_ids[1])
                    @foreach($users as $idx => $userObj)
                        @if($userObj->user->id == $dialog[0]->occupants_ids[1])
                            {{ $userObj->user->full_name }}
                            @break
                        @endif
                    @endforeach
                @endisset
                </span> | Seller: <span class="font-semiBold">
                    @isset ($dialog[0]->occupants_ids[2])
                        @foreach($users as $idx => $userObj)
                            @if($userObj->user->id == $dialog[0]->occupants_ids[2])
                                {{ $userObj->user->full_name }}
                                @break
                            @endif
                        @endforeach
                    @endisset
                </span>
            </p>          
        </div>

        <div class="col-12 col-lg-6 mb-4">
            <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                <div class="search-container d-flex mb-2">
                    <img src="{{ asset('images/ic_search.svg') }}" alt="">
                    <input class="" type="text" name="" placeholder="Search by message">
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">No</th>
                        <th scope="col">Sender</th>
                        <th scope="col">Date Sent</th>
                        <th scope="col">Message</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="list-group">
                        @foreach ($messages->items as $idx => $message)
                        <tr>
                            <td>{{ $idx + 1 }}</td>
                            <td>
                                @foreach($users as $idx => $userObj)
                                    @if($userObj->user->id == $message->sender_id)
                                        {{ $userObj->user->login }}
                                        @break
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ date("d F Y H:i:s", $message->date_sent) }}</td>
                            <td>{{ $message->message }}</td>
                            <td>
                                <a class="btn btn-danger" href="{{ url()->current().'/'.$message->_id }}">
                                    Delete
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        {{-- @include('items.chat_item', ["dialogs" => $dialogs]) --}}
                    </tbody>
                </table>
            </div>
        </div>

        
        
    </div>
</section>
@endsection