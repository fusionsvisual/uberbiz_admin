<li class="list-item-image">
    <div class="d-inline-block mr-3" style="width: 155px; height: 155px; border-radius:10px; position: relative; @if($index == 0) margin-left: 2rem; @else margin-right: 2rem; @endif">
        <div class="d-flex flex-column">
            <button class="btn ml-auto p-0" type="button" style="width:30px; height:30px; background:#888E9E; margin-right: 15px; border-radius:50px !important; z-index:999;" data-index="{{ $index }}" onclick="deleteImageAction(this);">
                <div class="d-flex ml-auto" style="">
                    <img src="{{ asset('images/ic_times.svg') }}" alt="" class="m-auto" style="width: 20px; height: 20px;">
                </div>
            </button>
            <img src="{{ $src }}" alt="" style="width: 125px; height: 125px; margin-top:-15px; border-radius: 10px;"> 
            <input type="hidden" name="image[]" value="{{ $src }}">
        </div>
    </div>
</li>