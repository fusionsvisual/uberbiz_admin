<li class="list-item-image list-item-placeholder">
    <div class="d-inline-block" style="width: 125px; height: 125px; border-radius:10px; position: relative; @if($index == 0) margin-left: 2rem; @else margin-right: 2rem; @endif margin-top:15px;">
        <div class="custom-input-file d-flex flex-column disabled w-100 h-100">
            <div class="content">
                <div class="content-title">
                    <span>Add<br>Photo</span>
                </div>
            </div>
            <label for="image"></label>
            <input id="image" type="file" name="image" data-index="{{ $index }}" onchange="uploadImageAction(this)">
        </div>
    </div>
</li>