@extends('layouts.sidebar')
@section('wrapper-content')
    <section>
        <div class="row">
            @if(Session::has('response'))   
                <div class="col-12">
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif
            
            <div class="col-12 col-lg-6 mb-4">
                <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                    <div class="search-container d-flex mb-2">
                        <img src="{{ asset('images/ic_search.svg') }}" alt="">
                        <input class="input-search" type="text" name="username" placeholder="Search by name">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody id="list-group">
                            @if (isset($buyers))   
                                @include('items/buyer_item', [
                                    "buyers" => $buyers
                                ])
                            @endif
                        </tbody>
                        @include('items.shimmers.shimmer_buyers_item')
                    </table>
                </div>
            </div>

            <div class="col-12">
                <div class="container-pagination">
                    @if (isset($buyers))   
                        @include('templates/pagination', [
                            'total_page' => ceil($buyers->total/ $buyers->per_page), 
                            'total_per_page' => $buyers->per_page,
                            'current_page' => $buyers->current_page
                        ])
                    @endif
                </div>
            </div>
            
        </div>
    </section>
@endsection

@section('wrapper-script')
    @if (isset($buyers))   
        <script>
            $url = "{{ url('buyers/filter') }}"
            $totalPage = "{{ ceil($buyers->total/ $buyers->per_page) }}"
            $totalPerPage = "{{ $buyers->per_page }}"
            $currentPage = "{{ $buyers->current_page }}"

            setupSearchListener()
            setupPagination();
        </script>
    @endif
@endsection