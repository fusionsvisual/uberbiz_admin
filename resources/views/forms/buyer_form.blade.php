@extends('layouts.sidebar')
@section('wrapper-content')
<section id="section-form" style="height: 100%">
    <div class="h-100" style="min-height: 100%">
        <form class="d-flex h-100" action="{{ url()->current()."/update" }}" method="POST">
            @csrf
            <div class="d-flex flex-column my-auto" style="width:100%; min-height: 85%">
                @if(Session::has('response'))   
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                @endif
                <div class="flex-fill mb-4" style="background:white; ">
                    <div class="header-form d-flex justify-content-between">
                        <div>
                            <h5 class="ml-4 my-3">Buyer Details</h5>
                        </div>

                        <button id="btn-edit" class="btn btn-primary">
                            <div class="d-flex">
                                <img class="my-auto mr-2" src="{{ asset('images/ic_pencil.svg') }}" alt="" width="14px" height="14px">
                                <span>Edit</span>
                            </div>
                        </button>
                    </div>
                    <div class="content-form">
                        <div class="row mb-3 mb-xl-4">
                            <div class="col-12 col-xl-6 mb-3 mb-xl-0">
                                <p class="form-label">Name</p>
                                <input type="text" name="username" id="" value="{{ $buyer->username }}" required disabled>
                            </div>

                            <div class="col-12 col-xl-6">
                                <p class="form-label">Date of Birth</p>
                                <input type="text" name="birthdate" value="@if ($buyer->date_of_birth) {{ date_format(date_create($buyer->date_of_birth), "d F Y") }} @endif" id="" onkeypress="return false;" required disabled>
                            </div>
                        </div>

                        <div class="row mb-3 mb-xl-4">
                            <div class="col-12 col-xl-6 mb-3 mb-xl-0">
                                <p class="form-label">Email</p>
                                <input type="email" name="email" value="{{ $buyer->email }}" id="" required disabled>
                            </div>

                            <div class="col-12 col-xl-6">
                                <p class="form-label">Phone Number</p>
                                <input type="tel" name="phone_number" value="{{ $buyer->phone_number }}" id="" disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-xl-6">
                                <p class="form-label mb-2">Gender</p>
                                <div class="d-flex">
                                    <div class="d-flex mr-4">
                                        <button class="btn btn-radio p-0" data-value="MALE">
                                            <div class="radio-d mr-3 @if($buyer->gender != "FEMALE") selected @endif disabled" style="width:20px; height:20px">
                                                <div class="radio-inner-d"></div>
                                            </div>
                                        </button>
                                        <p class="my-auto">Male</p>
                                    </div>

                                    <div class="d-flex">
                                        <button class="btn btn-radio p-0" data-value="FEMALE">
                                            <div class="radio-d mr-3 @if($buyer->gender == "FEMALE") selected @endif disabled" style="width:20px; height:20px">
                                                <div class="radio-inner-d"></div>
                                            </div>
                                        </button>
                                        <p class="my-auto">Female</p>
                                    </div>
                                    <input type="hidden" name="gender" value="MALE">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex mb-5 mb-xl-0">
                    <div class="mx-auto ml-xl-auto mr-xl-0">
                        <a id="btn-ban" class="btn mb-3 mb-xl-0 d-block d-xl-inline-block @if($buyer->is_banned) btn-done @else btn-danger @endif mr-3" href="@if($buyer->is_banned) {{ url('buyers/'.$buyer->id.'/status/unban') }} @else {{ url('buyers/'.$buyer->id.'/status/ban') }} @endif">
                            <div class="h-100 d-flex">
                                <span class="m-auto">
                                    @if($buyer->is_banned)
                                        Unban
                                    @else
                                        Ban
                                    @endif
                                </span>
                            </div>
                        </a>
                        <button id="btn-save" class="btn d-block d-xl-inline-block btn-primary" disabled>
                            Save
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</section>
@endsection

@section('wrapper-script')
<script>
    $('#btn-edit').click(function (e) {
        $(this).remove();
        $('input').removeAttr('disabled');
        $('#btn-save').removeAttr('disabled');
        $('.radio-d').removeClass('disabled');

        $('.btn-radio').click(function (e) {
            e.preventDefault();

            $selectedGender = $(this).data('value');
            $('input[name="gender"]').val($selectedGender);
            $('.radio-d').removeClass('selected');
            $(this).children().addClass('selected');
        });
    });

    $('.btn-radio').click(function (e) {
        e.preventDefault();
    });

    $(document).ready(function(){
        $('input[name="birthdate"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top left",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            // $startDate = $(this).val();  
            // $page = 1;
            // filter()
        });
    });
</script>
@endsection
