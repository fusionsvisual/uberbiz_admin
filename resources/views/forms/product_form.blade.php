@extends('layouts.sidebar')
@section('wrapper-content')
<section id="section-form">
    <a class="btn btn-primary mb-4" href="{{ url('products') }}">
        <i class="fas fa-chevron-left"></i>
        Back
    </a>

    <form action="{{ url()->current()."/update" }}" method="POST">
        @csrf
        @include('forms.templates.form_master', [
            "title" => "Product Details",
            "content" => "forms.contents.product_form_content"
        ])
        <div class="d-flex mb-5 mb-xl-0">
            <div class="mx-auto ml-xl-auto mr-xl-0">
                <a id="btn-ban" class="btn mb-3 mb-xl-0 d-block d-xl-inline-block">
                    <div class="h-100 d-flex">
                        <span class="m-auto">
                            
                        </span>
                    </div>
                </a>
                <button id="btn-save" class="btn d-block d-xl-inline-block btn-primary" disabled>
                    Save
                </button>
            </div>
        </div>
    </form>
</section>
@endsection

@section('wrapper-script')
    <script>
        
        // console.log();
        // console.log($('#container-img')[0].scrollWidth);

        $(document).ready(function () {
            let containerImageWidth = $('#container-img')[0].scrollWidth;
            if ($('.content-form').outerWidth() < containerImageWidth){
                $('#clue-scroll').css('opacity', 1);
            }

            $('input[name="image"]').attr('disabled', true);
            $('.custom-input-file').addClass('disabled');
        });

        function uploadImageAction(e){
            var oFReader = new FileReader();
            oFReader.readAsDataURL($(e)[0].files[0]);
            oFReader.onload = function (oFREvent) {
                $.ajax({
                    type: "POST",
                    url: "{{ url('products/update_image_local') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "src": oFREvent.target.result,
                        "index": $(e).data('index')
                    },
                    dataType: "text",
                    success: function (response) {
                        $json = JSON.parse(response);
                        $('.list-item-placeholder').remove();

                        $('#list-product-image').append($json.image_item);
                        $('#list-product-image').append($json.image_placeholder_item);

                        // Upload Image to Server
                        var formData = new FormData();
                        formData.append('image', $(e)[0].files[0]);

                        $.ajax({
                            type: "POST",
                            url: "{{ url('products/upload_image') }}",
                            headers: {
                                'X-CSRF-Token': "{{ csrf_token() }}"
                            },
                            data: formData,
                            enctype : 'multipart/form-data',
                            contentType: false,
                            processData: false,
                            dataType: "text",
                            success: function (response) {
                                console.log(response);
                                $response = JSON.parse(response);
                                if ($response.success){
                                    $imageItem = $('.list-item-image').eq($(e).data('index'));
                                    $imageItem.find('input').val($response.data.image);
                                }else{

                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr);
                                console.log(thrownError);
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);
                        console.log(thrownError);
                    }
                });
            };
        }

        function deleteImageAction(e){
            $.ajax({
                type: "POST",
                url: "{{ url('products/delete_image_local') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "index": $(e).data('index')
                },
                dataType: "text",
                success: function (response) {
                    $('.list-item-placeholder').remove();

                    $('.list-item-image').eq($(e).data('index')).remove();
                    $('#list-product-image').append(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        }

        $('#btn-edit').click(function (e) { 
            e.preventDefault();
            $(this).remove();
            $('input, textarea, select').removeAttr('disabled')
            $('input[name="image"]').attr('disabled', false);
            $('.custom-input-file').removeClass('disabled');
            $('#btn-save').removeAttr('disabled');
        });

        $('select[name="category_id"]').change(function (e) { 
            $category_id = $(this).val();

            $('#btn-save').attr('disabled', true);

            $('select[name="subcategory_id"]').empty();
            $('select[name="subcategory_id"]').attr('disabled', true);

            $.ajax({
                type: "GET",
                url: "{{ url('sub_categories/get_all_subcategories') }}",
                data: {
                    "category_id": $category_id
                },
                dataType: "text",
                success: function (response) {
                    $('#btn-save').attr('disabled', false);
                    $('select[name="subcategory_id"]').attr('disabled', false);

                    $json = JSON.parse(response);
                    $.each($json.data, function (idx, val) { 
                        $option = $("<option>", {value: val.id, text: val.item_sub_category_name});
                        $('select[name="subcategory_id"]').append($option)
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        });
    </script>
@endsection
