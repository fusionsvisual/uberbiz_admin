<div class="d-flex mb-3">
    <span>Product Images</span>
    <div id="clue-scroll" style="opacity: 0">
        <span class="d-none d-xl-inline ml-auto mr-2">Scroll for more</span>
        <img class="my-auto ml-auto ml-xl-0" src="{{ asset('images/ic_next.svg') }}" alt="">
    </div>
</div>

<div id="container-img" class="hide-scrollbar mb-4" style="white-space: nowrap; overflow:scroll; margin-left: -2rem; margin-right: -2rem">
    <div id="content-img">
        <ul id="list-product-image" class="list-inline d-flex">
            @foreach ($product->item_images as $idx => $item_image)
                @include('product_form.items.product_image_item', ["src" => Network::get_asset_url().$item_image->image, "index" => $idx])
            @endforeach

            @include('product_form.items.product_image_placeholder_item', ["index" => count($product->item_images)])
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12 col-xl-6 mb-4">
        <p class="form-label">Product Name</p>
        <input type="text" name="name" value="{{ $product->item_name }}" required disabled>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Price</p>
        <input type="text" name="price" value="{{ $product->price }}" required disabled>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Weight</p>
        <input type="text" name="weight" value="{{ $product->weight }}" required disabled>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Minimum Order</p>
        <input type="text" name="minimum_order" value="{{ $product->minimum_order }}" required disabled>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Unit</p>
        <select name="unit_id" class="custom-select" disabled>
            @foreach ($units->data as $unit)
                <option value="{{ $unit->id }}" @if($product->unit->id == $unit->id) selected @endif>{{ $unit->unit_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Category</p>
        <select name="category_id" class="custom-select" disabled>
            @foreach ($categories->data as $category)
                <option value="{{ $category->id }}" @if($product->item_sub_category->item_category_id == $category->id) selected @endif>{{ $category->item_category_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Subcategory</p>
        <select name="subcategory_id" class="custom-select" disabled required>
            @foreach ($subcategories as $subcategory)
                <option value="{{ $subcategory->id }}" @if($product->item_sub_category->id == $subcategory->id) selected @endif>{{ $subcategory->item_sub_category_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-12 mb-4">
        <p class="form-label">Description</p>
        <textarea name="description" id="" rows="5" disabled>{{ $product->item_description }}</textarea>
    </div>
</div>