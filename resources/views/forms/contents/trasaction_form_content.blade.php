<div id="form-content-transaction">
    <div class="row">
        <div class="col-12 col-xl-6 mb-4">
            <p class="title text-muted">Product Name</p>
            <p>{{ $transaction->quotation->item->item_name }}</p>
        </div>
        <div class="col-12 col-xl-3 mb-4">
            <p class="title text-muted">Seller</p>
            <p>{{ $transaction->quotation->item->seller->store_name }}</p>
        </div>
        <div class="col-12 col-xl-3 mb-4">
            <p class="title text-muted">Buyer</p>
            <p>{{ $transaction->quotation->buyer_user->username }}</p>
        </div>
        <div class="col-12 col-xl-6 mb-4">
            <p class="title text-muted">Buyer Address</p>
            <p>Sedati Permai Jl. Camar DD-14, Sidoarjo</p>
        </div>
        <div class="col-12 col-xl-6 mb-4">
            <p class="title text-muted">Quantity</p>
            <p>{{ $transaction->quantity.' '.$transaction->quotation->item->unit->unit_name }}</p>
        </div>
        <div class="col-12 mb-4">
            <p class="title text-muted">Quotation Description</p>
            <p>{{ $transaction->description }}</p>
        </div>
        <div class="col-12 col-xl-9 mb-4">
            <p class="title text-muted">Transport Name</p>
            <p>
                @if($transaction->shipping_name)
                    {{ $transaction->shipping_name.' ('.$transaction->delivery_estimate.' Days)' }}
                @else
                    No Transport
                @endif
            </p>
        </div>
        <div class="col-12 col-xl-3 mb-4">
            <p class="title text-muted">Receipt</p>
            <button id="btn-see-receipt" class="btn p-0">
                <div class="d-flex">
                    <img class="my-auto mr-1" src="{{ asset('images/ic_attachment.svg') }}" alt="" width="18" height="18">
                    <p class="text-primary mb-0">See Receipt</p>
                </div>
            </button>
        </div>
        <div class="col-12 col-xl-9 mb-4">
            <p class="title text-muted">Transport Fee</p>
            <p>Rp{{ number_format($transaction->shipping_cost, 0, ',', '.') }}</p>
        </div>
        <div class="col-12 col-xl-3 mb-4">
            <p class="title text-muted">Insurance</p>
            <p>Rp{{ number_format($transaction->insurance, 0, ',', '.') }}</p>
        </div>
        <div class="col-12 col-xl-9 mb-4">
            <p class="title text-muted">Status</p>
            <p>{{ $transaction->status->status_name }}</p>
        </div>
        <div class="col-12 col-xl-3 mb-4">
            <p class="title text-muted">Total</p>
            <h4>Rp{{ number_format($transaction->total, 0, ',', '.') }}</h4>
        </div>
    </div>
</div>