<h6 class="mb-3">Store Profile Image</h6>
<div class="d-flex mb-4">
    <div class="mr-5" style="width: 150px; height: 150px;">
        <div class="img-square-container">
            <img src="@if($seller->store_image) {{ Network::get_asset_url().$seller->store_image }} @else {{ asset('images/img_no_image_square.png') }} @endif" alt=""  style="background: grey; border-radius:10px;">
        </div>
    </div>
    <div class="my-auto">
        <div class="mb-4">
            <p class="text-muted">Rating</p>
            <p>{{ $seller->rating }}/5</p>
        </div>
        <div class="">
            <p class="text-muted">Verification Status</p>
            <span><p>@if($seller->is_verified) Verified @else Unverified @endif</p></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-xl-6 mb-4">
        <p class="form-label">Store Name</p>
        <input type="text" name="store_name" value="{{ $seller->store_name }}" required disabled>
    </div>
    <div class="col-12 col-xl-6 mb-4">
        <p class="form-label">Balance</p>
        <input type="text" name="balance" value="{{ $seller->store_balance }}" required disabled>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">Province</p>
        <select name="province_id" class="custom-select" disabled>
            @foreach ($provinces as $province)
                <option value="{{ $province->id }}" @if($seller->store_address->province_id == $province->id) selected @endif>{{ $province->province_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-12 col-xl-3 mb-4">
        <p class="form-label">City</p>
        <select name="city_id" class="custom-select" disabled>
            @foreach ($cities as $city)
                <option value="{{ $city->id }}" @if($seller->store_address->city_id == $city->id) selected @endif>{{ $city->city_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-12 col-xl-6 mb-4">
        <p class="form-label">Phone Number</p>
        <input type="text" name="phone_number" value="" disabled>
    </div>
    <div class="col-12 mb-4">
        <p class="form-label">Description</p>
        <textarea name="description" id="" rows="5" disabled>{{ $seller->store_description }}</textarea>
    </div>
</div>