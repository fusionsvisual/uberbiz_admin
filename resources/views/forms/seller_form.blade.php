@extends('layouts.sidebar')
@section('wrapper-content')
<section id="section-form">
    <a class="btn btn-primary mb-4" href="{{ url('sellers') }}">
        <i class="fas fa-chevron-left"></i>
        Back
    </a>

    <form class="" action="{{ url()->current()."/update" }}" method="POST">
    @csrf
    @include('forms.templates.form_master', [
        "title" => "Store Details",
        "content" => "forms.contents.seller_form_content"
    ])

    <div class="d-flex mb-5 mb-xl-0">
        <div class="mx-auto ml-xl-auto mr-xl-0">
            <a id="btn-ban" class="btn mb-3 mb-xl-0 d-block d-xl-inline-block">
                <div class="h-100 d-flex">
                    <span class="m-auto">
                        
                    </span>
                </div>
            </a>
            <button id="btn-save" class="btn d-block d-xl-inline-block btn-primary" disabled>
                Save
            </button>
        </div>
    </div>
    </form>
</section>
@endsection

@section('wrapper-script')
    <script>
        $('#btn-edit').click(function (e) { 
            e.preventDefault();
            $('input, select, textarea').removeAttr('disabled')
            $('#btn-save').removeAttr('disabled');
        });

        $('select[name="province_id"]').change(function (e) { 

            $('select[name="city_id"]').empty();
            $('select[name="city_id"]').attr('disabled', true);

            $('#btn-save').attr('disabled', true);

            $province_id = $(this).val();

            $.ajax({
                type: "GET",
                url: "{{ url('global/get_all_cities') }}",
                data: {
                    "province_id": $province_id
                },
                dataType: "text",
                success: function (response) {
                    $('#btn-save').attr('disabled', false);
                    $('select[name="city_id"]').attr('disabled', false);

                    $json = JSON.parse(response);
                    $.each($json, function (idx, val) { 
                        $option = $("<option>", {value: val.id, text: val.city_name});
                        $('select[name="city_id"]').append($option)
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        });
    </script>
@endsection
