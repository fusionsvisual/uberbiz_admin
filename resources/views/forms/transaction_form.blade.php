@extends('layouts.sidebar')
@section('wrapper-content')
<section id="section-form">
    <div class="popup-container d-none">
        <div class="popup-overlay"></div>
        <div class="popup-content-container">
            <div class="popup-content" style="background: transparent;">
                <div class="page-content-inner" style="background: transparent">
                    <img src="{{ Network::get_asset_url().$transaction->payment_receipt }}" alt="" width="500px" height="500px">
                </div>
            </div>
        </div>
    </div>

    <a class="btn btn-primary mb-4" href="{{ url('transactions') }}">
        <i class="fas fa-chevron-left"></i>
        Back
    </a>
        @include('forms.templates.form_master', [
            "title" => "Transaction Details",
            "content" => "forms.contents.trasaction_form_content"
        ])

        <div class="d-flex mb-5 mb-xl-0">
            <div class="mx-auto ml-xl-auto mr-xl-0">
                <a id="btn-ban" class="btn mb-3 mb-xl-0 d-block d-xl-inline-block">
                    <div class="h-100 d-flex">
                        <span class="m-auto">
                            
                        </span>
                    </div>
                </a>
                <form class="d-block d-xl-inline-block" action="{{ url()->current()."/reject" }}" method="GET">
                    <button id="btn-reject" class="btn btn-danger" disabled>Reject</button>
                </form>
                <form class="d-block d-xl-inline-block" action="{{ url()->current()."/accept" }}" method="GET">
                    <button id="btn-accept" class="btn btn-done" disabled>Accept</button>
                </form>
            </div>
        </div>
    
</section>
@endsection

@section('wrapper-script')
    <script>

        $('.popup-overlay').click(function (e) { 
            $('.popup-container').addClass('d-none');
        });

        $('#btn-edit').click(function (e) { 
            e.preventDefault();
            $(this).remove();
            $('#btn-reject, #btn-accept').removeAttr('disabled');
        });

        $('#btn-see-receipt').click(function (e) { 
            e.preventDefault();
            $('.popup-container').removeClass('d-none');
        });

    </script>
@endsection
