@extends('layouts.sidebar')

@section('wrapper-content')
<section>
    <div class="row">
        <div class="col-12 col-lg-6 mb-4">
            <div style="border-bottom: 1px solid rgba(165, 165, 165, 1)">
                <div class="search-container d-flex mb-2">
                    <img src="{{ asset('images/ic_search.svg') }}" alt="">
                    <input class="input-search" type="text" name="" placeholder="Search by name">
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Buyer Name</th>
                            <th scope="col">Seller Name</th>
                            <th scope="col">Last message date sent</th>
                            <th scope="col">Last Message</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="list-group">
                        @include('items.chat_item', ["dialogs" => $dialogs])
                    </tbody>
                    @include('items.shimmers.shimmer_chat_item')
                </table>
            </div>
        </div>

        <div class="col-12">
            <div class="container-pagination">
                @include('templates/pagination', [
                    'total_page' => ceil($dialogs->total_entries/ $dialogs->limit), 
                    'total_per_page' => $dialogs->limit,
                    'current_page' => (($dialogs->skip/$dialogs->limit) + 1)
                ])
            </div>
        </div>
        
    </div>
</section>
@endsection

@section('wrapper-script')
    <script>
        $url = "{{ url('chats/filter') }}"
        $totalPage = "{{ ceil($dialogs->total_entries/ $dialogs->limit) }}"
        $totalPerPage = "{{ $dialogs->limit}}"
        $currentPage = "{{ (($dialogs->skip/$dialogs->limit) + 1) }}"

        setupSearchListener();
        setupPagination();
    </script>
@endsection