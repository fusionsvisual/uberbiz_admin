<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Uberbiz Admin</title>

    {{-- STYLE --}}
    <link rel="stylesheet" href="{{ asset("css/bootstrap/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bootstrap/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bootstrap/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/shimmer.css") }}">
    <link rel="stylesheet" href="{{ asset("css/style.css") }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>    

    {{-- SCRIPT --}}
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
</head>
<body>
    @yield('content')

    <script src="{{ asset('js/pagination.js') }}"></script>
    @yield('script')
</body>
</html>