@extends('layouts.master')
@section('content')
    <div id="navbar" class="d-block d-xl-none">
        <button id="btn-navbar" class="btn p-0">
            <div class="navbar-content-container">
                <img src="{{ asset('images/ic_bars.svg') }}" alt="" width="30px" height="30px">
            </div>
        </button>
    </div>
    <div class="d-flex h-100">
        <div id="sidebar">
            <div class="d-flex mb-5">
                <span>
                    <img src="{{ asset('images/ic_logo_long.svg') }}" style="max-width:200px;" alt="">
                </span>
                <span class="d-block d-xl-none my-auto ml-auto">
                    <button id="btn-close-sidebar" class="btn p-0">
                        <img src="{{ asset('images/ic_prev.svg') }}" style="width:35px;" alt="">
                    </button>
                </span>
            </div>
            <div class="d-flex mb-4">
                <div style="width: 65px">
                    <div class="img-square-container">
                        <img class="img-rounded" src="{{ asset('images/img_profile_placeholder.png') }}" alt="">
                    </div>
                </div>
                <div class="ml-3 my-auto">
                    <p class="content-username mb-2">{{ Session::get('user.data')->username }}</p>
                    <p class="text-muted">Admin</p>
                </div>
            </div>
            <div id="sidebar-menu" class="flex-fill">
                <a href="{{ url('transactions') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "transactions") active @endif">
                        @if (Request::segment(1) == "transactions")
                        <img class="menu-icon" src="{{ asset('images/ic_transaction_history_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_transaction_history.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Transaction History</p>
                    </div>
                </a>
                <a href="{{ url('buyers') }}">
                    <div class="menu-container d-flex @if(Request::segment(1)== "buyers") active @endif">
                        @if (Request::segment(1) == "buyers")
                            <img class="menu-icon" src="{{ asset('images/ic_buyer_list_active.svg') }}" alt="">
                        @else
                            <img class="menu-icon" src="{{ asset('images/ic_buyer_list.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Buyer List</p>
                    </div>
                </a>
                <a href="{{ url('sellers') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "sellers") active @endif">
                        @if (Request::segment(1) == "sellers")
                            <img class="menu-icon" src="{{ asset('images/ic_seller_list_active.svg') }}" alt="">
                        @else
                            <img class="menu-icon" src="{{ asset('images/ic_seller_list.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Seller List</p>
                    </div>
                </a>
                <a href="{{ url('request_quotations') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "request_quotations") active @endif">
                        @if (Request::segment(1) == "request_quotations")
                        <img class="menu-icon" src="{{ asset('images/ic_request_quotation_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_request_quotation.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Request Quotation</p>
                    </div>
                </a>
                <a href="{{ url('quotations') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "quotations") active @endif">
                        @if (Request::segment(1) == "quotations")
                        <img class="menu-icon" src="{{ asset('images/ic_received_quotation_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_received_quotation.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Received Quotation</p>
                    </div>
                </a>
                <a href="{{ url('products') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "products") active @endif">
                        @if (Request::segment(1) == "products")
                        <img class="menu-icon" src="{{ asset('images/ic_product_list_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_product_list.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Product List</p>
                    </div>
                </a>
                <a href="{{ url('categories') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "categories") active @endif">
                        @if (Request::segment(1) == "categories")
                        <img class="menu-icon" src="{{ asset('images/ic_category_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_category.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Category List</p>
                    </div>
                </a>
                <a href="{{ url('sub_categories') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "sub_categories") active @endif">
                        @if (Request::segment(1) == "sub_categories")
                        <img class="menu-icon" src="{{ asset('images/ic_product_list_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_sub_category.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Sub-Category List</p>
                    </div>
                </a>
                <a href="{{ url('chats') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "chats") active @endif">
                        @if (Request::segment(1) == "chats")
                        <img class="menu-icon" src="{{ asset('images/ic_product_list_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_chat.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Chat History</p>
                    </div>
                </a>
                <a href="{{ url('withdraws') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "withdraws") active @endif">
                        @if (Request::segment(1) == "withdraws")
                        <img class="menu-icon" src="{{ asset('images/ic_withdrawal_request_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_withdrawal_request.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Withdraw Request</p>
                    </div>
                </a>
                <a href="{{ url('store_verification') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "store_verification") active @endif">
                        @if (Request::segment(1) == "store_verification")
                        <img class="menu-icon" src="{{ asset('images/ic_store_verification_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_store_verification.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Store Verification</p>
                    </div>
                </a>
                <a href="{{ url('banners') }}">
                    <div class="menu-container d-flex @if(Request::segment(1) == "banners") active @endif">
                        @if (Request::segment(1) == "banners")
                        <img class="menu-icon" src="{{ asset('images/ic_product_list_active.svg') }}" alt="">
                        @else
                        <img class="menu-icon" src="{{ asset('images/ic_promotial_banner.svg') }}" alt="">
                        @endif
                        <p class="menu-title my-auto ml-3">Promotional Banner</p>
                    </div>
                </a>
                <a class="mt-auto" href="{{ url('logout') }}">
                    <div class="menu-container d-flex mb-4">
                        <img class="menu-icon" src="{{ asset('images/ic_exit.svg') }}" alt="">
                        <p class="menu-title text-danger my-auto ml-3">Exit</p>
                    </div>
                </a>
            </div>
        </div>
        <div id="wrapper">
            @yield('wrapper-content')
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(window).resize(function () { 
            if (window.innerWidth < 1140) {
                $('#sidebar').css('margin-left', '-100%');
            } else {
                $('#sidebar').css('margin-left', 0);
            }
        });
        $('#btn-navbar').click(function (e) { 
            $('#sidebar').css('margin-left', 0);
        });

        $('#btn-close-sidebar').click(function (e) { 
            $('#sidebar').css('margin-left', '-100%');
        });
    </script>
    @yield('wrapper-script')
@endsection