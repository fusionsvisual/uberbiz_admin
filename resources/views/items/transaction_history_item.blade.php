@foreach($histories->data as $idx => $history)
    <tr>
        <td>{{ $idx + 1 }}</th>
        <td>{{ date("d F Y", strtotime($history->created_at)) }}</td>
        <td>{{ $history->invoice_code }}</td>
        <td>{{ "-" }}</td>
        <td>
            <a class="btn btn-primary" href="{{ url('transactions/'.$history->id) }}">
                Detail
            </a>
        </td>
    </tr>
@endforeach
