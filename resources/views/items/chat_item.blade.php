@foreach($dialogs->items as $idx => $dialog)
    <tr>
        <td>{{ (($dialogs->skip/$dialogs->limit) + 1) * ($idx + 1) }}</th>
        <td>
            @isset ($dialog->occupants_ids[1])
                @foreach($users as $idx => $userObj)
                    @if($userObj->user->id == $dialog->occupants_ids[1])
                        {{ $userObj->user->login }}
                        @break
                    @endif
                @endforeach
            @endisset
        </td>
        <td>
            @isset ($dialog->occupants_ids[2])
                @foreach($users as $idx => $userObj)
                    @if($userObj->user->id == $dialog->occupants_ids[2])
                        {{ $userObj->user->login }}
                        @break
                    @endif
                @endforeach
            @endisset
        </td>
        <td>
            @if(isset($dialog->last_message_date_sent))
                {{ date("d F Y H:i:s", $dialog->last_message_date_sent) }}
            @else
                -
            @endif
        </td>
        <td>
            @if($dialog->last_message)
                {{ $dialog->last_message }}
            @else
                -
            @endif
        </td>
        <td class="column-action">
            <a class="btn btn-primary" href="{{ url('chats/'.$dialog->_id) }}">
                Detail
            </a>
            <a class="btn btn-danger" href="{{ url('chats/delete/'.$dialog->_id) }}">
                Delete
            </a>
        </td>
    </tr>
@endforeach
