@foreach($buyers->data as $idx => $buyer)
    <tr>
        <td>{{ (($buyers->current_page - 1) * $buyers->per_page) + ($idx + 1) }}</th>
        <td>{{ $buyer->username }}</td>
        <td>{{ $buyer->email }}</td>
        <td>{{ $buyer->phone_number }}</td>
        <td>
            @if ($buyer->is_banned)
                BANNED
            @else
                ACTIVE
            @endif
        </td>
        <td class="column-action">
            <a class="btn btn-primary" href="{{ url('buyers/'.$buyer->id) }}">
                Detail
            </a>
            <a class="btn  @if($buyer->is_banned) btn-outline-done @else btn-outline-danger @endif" href="@if($buyer->is_banned) {{ url('buyers/'.$buyer->id.'/status/unban') }} @else {{ url('buyers/'.$buyer->id.'/status/ban') }} @endif">
                @if($buyer->is_banned)
                    Unban
                @else
                    Ban
                @endif
                
            </a>
        </td>
    </tr>
@endforeach
