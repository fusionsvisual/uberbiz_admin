@foreach($sellers->data as $idx =>$seller)
    <tr>
        <td>{{ (($sellers->current_page - 1) * $sellers->per_page) + ($idx + 1) }}</td>
        <td>{{ $seller->store_name }}</td>
        <td>
            <div class="d-flex">
                <p class="my-auto mr-3" style="height:12px; width:24px">{{ $seller->rating }}</p>
                @for ($i = 0; $i < floor($seller->rating); $i++)
                    <img src="{{ asset('images/ic_star_active.svg') }}" alt="" width="24px" height="24px">
                @endfor
                @for ($i = 0; $i < 5 - $seller->rating; $i++)
                    <img src="{{ asset('images/ic_star_muted.svg') }}" alt="" width="24px" height="24px">
                @endfor
            </div>
        </td>
        <td>
            @if ($seller->is_banned)
                BANNED
            @else
                ACTIVE
            @endif
        </td>
        <td class="column-action">
            <a class="btn btn-primary" href="{{ url('sellers/'.$seller->id) }}">
                Detail
            </a>
            <a class="btn  @if($seller->is_banned) btn-outline-done @else btn-outline-danger @endif" href="@if($seller->is_banned) {{ url('sellers/'.$seller->id.'/status/unban') }} @else {{ url('sellers/'.$seller->id.'/status/ban') }} @endif">
                @if($seller->is_banned)
                    Unban
                @else
                    Ban
                @endif
                
            </a>
        </td>
    </tr>
@endforeach

