@foreach($requests->data as $idx =>$request)
    <tr>
        <td>{{ (($requests->current_page - 1) * $requests->per_page) + ($idx + 1) }}</td>
        <td>{{ date("d F Y", strtotime($request->created_at)) }}</td>
        <td>{{ $request->title }}</td>
        <td>{{ $request->item_sub_category->item_sub_category_name }}</td>
        <td>{{ $request->buyer_user->username }}</td>
        <td style="width:15%">
            <a class="btn btn-outline-danger" href="{{ url('request_quotations/delete/'.$request->id) }}">
                Delete
            </a>
        </td>
    </tr>
@endforeach

