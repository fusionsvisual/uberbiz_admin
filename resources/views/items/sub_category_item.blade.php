@foreach($sub_categories->data as $idx =>$sub_category)
    <tr>
        <td>{{ (($sub_categories->current_page - 1) * $sub_categories->per_page) + ($idx + 1) }}</td>
        <td>{{ $sub_category->item_category->item_category_name }}</td>
        <td class="subcategory_name">{{ $sub_category->item_sub_category_name }}</td>
        <td>
            <div style="width: 200px; height:200px;">
                <div class="d-square-container" style="border-radius: 10px;">
                    <img class="img-preview" src="@if(isset($sub_category->item_sub_category_image)){{ Network::get_asset_url().$sub_category->item_sub_category_image }}@else {{ asset('images/img_no_image_square.png') }}@endif" alt="">
                    <input type="hidden" name="image_url" value="@isset($sub_category->item_sub_category_image ){{ $sub_category->item_sub_category_image }}@endisset">
                </div>
            </div>
        </td>
        <td class="column-action">
            <button class="btn btn-edit list-btn-edit mr-2" data-idx="{{ $idx }}" data-id="{{ $sub_category->id }}" data-category-id="{{ $sub_category->item_category->id }}">
                Edit
            </button>
            <a class="btn btn-outline-danger" href="{{ url('sub_categories/delete/'.$sub_category->id) }}">
                Delete
            </a>
        </td>
    </tr>
@endforeach
