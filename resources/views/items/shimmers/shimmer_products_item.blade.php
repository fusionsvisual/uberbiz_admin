<tbody id="tbody-shimmer" class="d-none">
    @for ($i = 0; $i < 5; $i++)
        <tr>
            <td>
                <div class="shimmer" style="height: 50px; width:50px;"></div>
            </td>
            <td>
                <div class="shimmer" style="height: 50px; width:200px;"></div>
            </td>
            <td>
                <div class="shimmer" style="height: 50px; width:200px;"></div>
            </td>
            <td>
                <div class="shimmer" style="height: 50px; width:200px;"></div>
            </td>
            <td>
                <div class="shimmer" style="height: 50px; width:200px;"></div>
            </td>
            <td>
                <div class="shimmer" style="height: 50px; width:200px;"></div>
            </td>
        </tr>
    @endfor
</tbody>