@foreach($withdraws->data as $idx =>$withdraw)
    <tr>
        <td>{{ (($withdraws->current_page - 1) * $withdraws->per_page) + ($idx + 1) }}</td>
        <td>{{ date("d F Y", strtotime($withdraw->created_at)) }}</td>
        <td>{{ $withdraw->seller->store_name }}</td>
        <td>Rp{{ number_format($withdraw->amount, 0, ',', '.') }}</td>
        <td>{{ $withdraw->status->status_name }}</td>
        <td class="column-action">
            <a class="btn btn-done mr-2" href="{{ url('withdraws/'.$withdraw->id.'/accept') }}">
                Release
            </a>
            <a class="btn btn-outline-danger" href="{{ url('withdraws/'.$withdraw->id.'/reject') }}">
                Reject
            </a>
        </td>
    </tr>
@endforeach
