@foreach($products->data as $idx =>$product)
    <tr>
        <td>{{ (($products->current_page - 1) * $products->per_page) + ($idx + 1) }}</td>
        <td>{{ $product->item_name }}</td>
        <td>{{ $product->seller->store_name }}</td>
        <td>{{ $product->sold }}</td>
        <td>Rp{{ number_format($product->price, 0, ',', '.') }}</td>
        <td class="column-action">
            <a class="btn btn-edit mr-2" href="{{ url('products/'.$product->id) }}">
                Edit
            </a>
            <a class="btn btn-outline-danger" href="{{ url('products/delete/'.$product->id) }}">
                Delete
            </a>
        </td>
    </tr>
@endforeach
