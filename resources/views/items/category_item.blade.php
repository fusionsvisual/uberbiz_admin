@foreach($categories->data as $idx =>$category)
    <tr>
        <td>{{ (($categories->current_page - 1) * $categories->per_page) + ($idx + 1) }}</td>
        <td>{{ $category->item_category_name }}</td>
        <td>
            <div style="width: 200px; height:200px;">
                <div class="d-square-container" style="border-radius: 10px;">
                    <img class="img-preview" src="@if(isset($category->item_category_image)){{ Network::get_asset_url().$category->item_category_image }}@else {{ asset('images/img_no_image_square.png') }}@endif" alt="">
                    <input type="hidden" name="image_url" value="@isset($category->item_category_image ){{ $category->item_category_image }}@endisset">
                </div>
            </div>
        </td>
        <td class="column-action">
            <button class="btn btn-edit list-btn-edit mr-2" data-idx="{{ $idx }}" data-id="{{ $category->id}}">
                Edit
            </button>
            <a class="btn btn-outline-danger" href="{{ url('categories/delete/'.$category->id) }}">
                Delete
            </a>
            <input type="hidden" name="category_old_name" value="{{ $category->item_category_name }}">
        </td>
    </tr>
@endforeach
