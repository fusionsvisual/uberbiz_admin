@extends('layouts.master')
@section('content')
<div id="login">
    <div class="container-content">
        <img class="w-100 h-100" src="{{ asset('images/img_login_bg.png') }}" alt="" style="object-fit: cover;">
        <div class="container-title d-flex flex-column">
            <h1 class="content-title">Welcome<br>Back!</h1>
            <div class="d-flex d-sm-none w-100 flex-fill">
                <div class="container-form px-4 py-5" style="background: white">
                    <h3 class="font-semiBold mb-4">Log In</h3>
                    <p class="text-muted mb-4">Enter your email and password to login our dashboard</p>
                    <form action="">
                        <div class="container-input">
                            <img src="{{ asset('images/ic_email.svg') }}" alt="">
                            <input type="text" name="username" id="" placeholder="Username">
                        </div>
                        <div class="container-input">
                            <img src="{{ asset('images/ic_password.svg') }}" alt="">
                            <input class="mr-3" type="password" name="" id="" placeholder="Password">
                            <button class="btn btn-toggler-password p-0">
                                <img class="toggler-password mr-0" src="{{ asset('images/ic_eye.svg') }}" alt="">
                            </button>
                        </div>
                        <button class="btn btn-login btn-primary">
                            Log In
                        </button>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    <div class="container-content d-none d-sm-flex">
        <div class="container-form my-auto">
            <h1 class="font-semiBold mb-4">Log In</h1>
            <p class="text-muted mb-5">Enter your email and password to login our dashboard</p>

            <form action="{{ url('login') }}" method="POST">
                @csrf
                <div class="container-input">
                    <img src="{{ asset('images/ic_email.svg') }}" alt="">
                    <input type="text" name="username" id="" placeholder="Username">
                </div>
                <div class="container-input">
                    <img src="{{ asset('images/ic_password.svg') }}" alt="">
                    <input class="mr-3" type="password" name="password" id="" placeholder="Password">
                    <button class="btn btn-toggler-password p-0">
                        <img class="toggler-password mr-0" src="{{ asset('images/ic_eye.svg') }}" alt="">
                    </button>
                </div>
                <button class="btn btn-login btn-primary">
                    Log In
                </button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('input[type="text"]').focus(function() {
            $(this).prev().attr('src', "{{ asset('images/ic_email_active.svg') }}");
            $(this).parent().addClass('focus');
        });

        $('input[type="text"]').focusout(function() {
            if ($(this).val() == ""){
                $(this).prev().attr('src', "{{ asset('images/ic_email.svg') }}");
                $(this).parent().removeClass('focus');
            }
        });

        $('input[type="password"]').focus(function() {
            $(this).prev().attr('src', "{{ asset('images/ic_password_active.svg') }}");
            if ($(this).attr("type") == "password"){
                $(this).next().children().attr('src', "{{ asset('images/ic_eye_active.svg') }}");
            }else{
                $(this).next().children().attr('src', "{{ asset('images/ic_eye_slash_active.svg') }}");
            }
            $(this).parent().addClass('focus');
        });

        $('input[type="password"]').focusout(function() {
            if ($(this).val() == ""){
                $(this).prev().attr('src', "{{ asset('images/ic_password.svg') }}");
                if ($(this).attr("type") == "password"){
                    $(this).next().children().attr('src', "{{ asset('images/ic_eye.svg') }}");
                }else{
                    $(this).next().children().attr('src', "{{ asset('images/ic_eye_slash.svg') }}");
                }
                $(this).parent().removeClass('focus');
            }
        });

        $('.btn-toggler-password').click(function (e) { 
            e.preventDefault()
            if ($(this).prev().attr("type") == "password"){
                $(this).prev().attr("type", "text");
                if ($(this).prev().val() == ""){
                    $(this).children().attr('src', "{{ asset('images/ic_eye_slash.svg') }}");
                }else{
                    $(this).children().attr('src', "{{ asset('images/ic_eye_slash_active.svg') }}");
                }
            }else{
                $(this).prev().attr("type", "password");
                if ($(this).prev().val() == ""){
                    $(this).children().attr('src', "{{ asset('images/ic_eye.svg') }}");
                }else{
                    $(this).children().attr('src', "{{ asset('images/ic_eye_active.svg') }}");
                }
            }
        });
    </script>
@endsection