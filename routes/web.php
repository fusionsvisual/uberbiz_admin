<?php

use App\Http\Controllers\BannerController;
use App\Http\Controllers\BuyerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\GlobalController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RequestQuotationController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\StoreVerificationController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WithdrawController;
use App\Http\Middleware\AdminMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/login', [UserController::class, 'login']);

Route::middleware([AdminMiddleware::class])->group(function () {
    Route::get('/', [UserController::class, 'index']);
    Route::get('/buyers', [UserController::class, 'buyer_read']);
    Route::get('/sellers', [UserController::class, 'seller_read']);

    Route::get('pagination_update', [UserController::class, 'pagination_update']);

    Route::prefix('/global')->group(function (){
        Route::get('/get_all_cities', [GlobalController::class, 'get_all_cities']);
    });

    Route::prefix('/transactions')->group(function (){
        Route::get('/', [TransactionController::class, 'index']);
        Route::get('/filter', [TransactionController::class, 'filter']);
        Route::get('/{id}', [TransactionController::class, 'detail']);
        Route::get('/{id}/{action}', [TransactionController::class, 'update']);
    });

    Route::prefix('/buyers')->group(function (){
        Route::get('/', [BuyerController::class, 'index']);
        Route::get('/filter', [BuyerController::class, 'filter']);
        Route::any('/{id}', [BuyerController::class, 'detail'])->where('id', '[0-9]+');;;
        Route::post('/{id}/update', [BuyerController::class, 'update']);
        Route::get('{id}/status/{action}', [BuyerController::class, 'update_status']);
        // Route::get('/{id}', [UserController::class, 'transaction_detail']);
    });

    Route::prefix('/sellers')->group(function (){
        Route::get('/', [SellerController::class, 'index']);
        Route::get('/filter', [SellerController::class, 'filter']);
        Route::any('/{id}', [SellerController::class, 'detail'])->where('id', '[0-9]+');
        Route::any('/{id}/update', [SellerController::class, 'update'])->where('id', '[0-9]+');
        Route::get('{id}/status/{action}', [SellerController::class, 'update_status']);
        // Route::get('/{id}', [UserController::class, 'transaction_detail']);
    });

    Route::prefix('/request_quotations')->group(function (){
        Route::get('/', [RequestQuotationController::class, 'index']);
        Route::get('/filter', [RequestQuotationController::class, 'filter']);
        Route::get('/delete/{id}', [RequestQuotationController::class, 'delete']);
    });

    Route::prefix('/products')->group(function (){
        Route::get('/', [ProductController::class, 'index']);
        Route::get('/filter', [ProductController::class, 'filter']);
        Route::get('/{id}', [ProductController::class, 'detail'])->where('id', '[0-9]+');
        Route::post('/{id}/update', [ProductController::class, 'update'])->where('id', '[0-9]+');
        Route::get('/delete/{id}', [ProductController::class, 'delete']);
        Route::get('/get_subcategories', [ProductController::class, 'get_subcategories']);
        Route::post('/update_image_local', [ProductController::class, 'update_image_local']);
        Route::post('/upload_image', [ProductController::class, 'upload_image']);
        Route::post('/delete_image_local', [ProductController::class, 'delete_image_local']);
    });

    Route::prefix('/categories')->group(function (){
        Route::get('/', [CategoryController::class, 'index']);
        Route::get('/filter', [CategoryController::class, 'filter']);
        Route::post('/create', [CategoryController::class, 'create']);
        Route::post('/{id}/update', [CategoryController::class, 'update']);
        Route::get('/delete/{id}', [CategoryController::class, 'delete']);
    });

    Route::prefix('/sub_categories')->group(function (){
        Route::get('/', [SubCategoryController::class, 'index']);
        Route::get('/filter', [SubCategoryController::class, 'filter']);
        Route::post('/create', [SubCategoryController::class, 'create']);
        Route::post('/update/{id}', [SubCategoryController::class, 'update']);
        Route::get('/delete/{id}', [SubCategoryController::class, 'delete']);
        Route::get('/get_categories', [SubCategoryController::class, 'get_categories']);
        Route::get('/get_all_subcategories', [SubCategoryController::class, 'get_all_subcategories']);

    });

    Route::prefix('/chats')->group(function (){
        Route::get('/', [ChatController::class, 'index']);
        Route::get('/filter', [ChatController::class, 'filter']);
        Route::get('/{id}', [ChatController::class, 'detail']);
        Route::get('/delete/{id}', [ChatController::class, 'delete']);
        Route::get('/delete/{id}/delete/{message_id}', [ChatController::class, 'delete_message']);

        // Route::get('/filter', [WithdrawController::class, 'filter']);
        // Route::get('/{id}/{action}', [WithdrawController::class, 'update']);
        // Route::get('/delete/{id}', [SubCategoryController::class, 'delete']);
    });

    Route::prefix('/withdraws')->group(function (){
        Route::get('/', [WithdrawController::class, 'index']);
        Route::get('/filter', [WithdrawController::class, 'filter']);
        Route::get('/{id}/{action}', [WithdrawController::class, 'update']);
        // Route::get('/{id}', [SubCategoryController::class, 'update']);
        // Route::get('/delete/{id}', [SubCategoryController::class, 'delete']);
    });

    Route::prefix('/store_verification')->group(function (){
        Route::get('/', [StoreVerificationController::class, 'index']);
        Route::get('/filter', [StoreVerificationController::class, 'filter']);
        Route::get('/{id}/{action}', [StoreVerificationController::class, 'update']);
    });

    Route::prefix('/banners')->group(function (){
        Route::get('/', [BannerController::class, 'index']);
        Route::post('/create', [BannerController::class, 'create']);
        Route::get('/delete/{id}', [BannerController::class, 'delete'])->where('id', '[0-9]+');;;
        // Route::get('/filter', [ProductController::class, 'filter']);
        // Route::get('/{id}', [ProductController::class, 'update']);
    });

    Route::get('/quotations', [UserController::class, 'quotation_read']);

    Route::get('/logout', [UserController::class, 'logout']);
});
